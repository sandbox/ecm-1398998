<?php
/**
 * @file
 * Functions for tests with type "Drop-down lists"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  $elements = $options['elements'];
  $sid = $options['sid'];
  $placeholder = '###';

  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = &$form_state['step'];

  $task = '';
  $format = NULL;

  // Set task and format as strings.
  if (!empty($form_state['stored_task'])) {
    $task = $form_state['stored_task']['value'];
    $format = $form_state['stored_task']['format'];
  }
  elseif (is_array($elements)) {
    foreach ($elements as $k => $v) {
      if (is_numeric($k)) {
        // This is to avoid unnecessary inserting of placeholder when
        // there are no selectors in the task.
        if ($k == 0 && !isset($v['prefix']) && isset($v['suffix'])) {
          $task = $v['suffix'];
        }
        else {
          $prefix = (isset($v['prefix'])) ? $v['prefix'] : '';
          $suffix = (isset($v['suffix'])) ? $v['suffix'] : '';
          $task .= $prefix . $placeholder . $suffix;
        }
      }
    }
    if (isset($elements['format'])) {
      $format = $elements['format'];
    }
  }

  // Set selectors options as strings.
  $count = substr_count($task, $placeholder);
  $selectors_opts = array();
  for ($i = 1; $i <= $count; $i++) {
    if (isset($form_state['stored_options'][$i])) {
      $selectors_opts[$i] = $form_state['stored_options'][$i];
    }
    elseif (is_array($elements) && isset($elements[$i]['options'])) {
      // Show options stored in database.
      foreach ($elements[$i]['options'] as $k => $v) {
        $correct = '';
        if ($v[1]) {
          $correct = '# ';
        }
        if (empty($selectors_opts[$i])) {
          $selectors_opts[$i] = $correct . $v[0];
        }
        else {
          $selectors_opts[$i] .= "\n" . $correct . $v[0];
        }
      }
    }
  }

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  if ($step == 1) {
    $title = t('Enter the task');
  }
  if ($step == 2) {
    $title = t('Enter the options for selectors');
  }

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $title,
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
  );

  if ($step == 1) {
    $form['fieldset']['task'] = array(
      '#type' => 'text_format',
      '#title' => t('Use ### as a placeholder for answer selectors.'),
      '#default_value' => $task,
      '#required' => FALSE,
      '#description' => t('At the next step you can enter answer options.'),
      '#format' => $format,
      '#suffix' => '<br /><br />',
    );
    $form['fieldset']['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#submit' => array('langtests_ddl_next'),
    );
    $form_state['stored_options'] = $selectors_opts;
  }
  elseif ($step == 2) {
    $show_task = check_markup($form_state['stored_task']['value'], $form_state['stored_task']['format']);
    $count = substr_count($show_task, $placeholder);

    if ($count > 0) {
      // Replace placeholder with the marker [SELECTOR N]
      for ($i = 1; $i <= $count; $i++) {
        $pos = strpos($show_task, $placeholder);
        $show_task = drupal_substr($show_task, 0, $pos) . '<strong>[' . t('SELECTOR') . ' ' . $i . ']</strong>' . drupal_substr($show_task, $pos + 3);
      }
    }

    $form['fieldset']['show_task'] = array(
      '#markup' => $show_task,
    );

    if ($count > 0) {
      for ($i = 1; $i <= $count; $i++) {
        $form['fieldset']['options'][$i] = array(
          '#type' => 'textarea',
          '#title' => t('Selector') . ' ' . $i,
          '#default_value' => isset($selectors_opts[$i]) ? $selectors_opts[$i] : '',
          '#description' => t('Enter each option on a separate line. Mark CORRECT answer with leading # symbol.'),
        );
      }
    }
    else {
      $form['fieldset']['no_selectors'] = array(
        '#markup' => t('There are no selectors in your task. Use ### as a placeholder for answer selectors.') . '<br />',
      );
    }

    $form['fieldset']['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Previous step'),
      '#submit' => array('langtests_ddl_prev'),
      '#prefix' => '<br />',
    );
  }

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];
  $step = $form_state['step'];

  $selectors_opts = array();

  if ($step == 1) {
    $raw_task = $form_state['values']['fieldset']['task']['value'];
    $format = $form_state['values']['fieldset']['task']['format'];
    if (isset($form_state['stored_options'])) {
      $selectors_opts = $form_state['stored_options'];
    }
  }
  elseif ($step == 2) {
    if (isset($form_state['stored_task'])) {
      $raw_task = $form_state['stored_task']['value'];
      $format = $form_state['stored_task']['format'];
    }
    $selectors_opts = $form_state['values']['fieldset']['options'];
  }

  $placeholder = '###';
  $count = substr_count($raw_task, $placeholder);

  $elements = array();

  // Make elements array.
  for ($i = 1; $i <= $count; $i++) {
    $pos = strpos($raw_task, $placeholder);
    $prefix = drupal_substr($raw_task, 0, $pos);
    $raw_task = drupal_substr($raw_task, $pos + 3);
    $elements[$i]['prefix'] = $prefix;
  }
  $elements[$count]['suffix'] = $raw_task;
  $elements['format'] = $format;

  $message = array();

  foreach ($selectors_opts as $k => $v) {
    // Discard last options when the number of selectors has been reduced.
    if ($k > $count) {
      break;
    }

    $opts = explode("\n", str_replace(array("\r\n", "\n\r"), "\n", $v));

    $i = 0; $c = 0;
    foreach ($opts as $v1) {
      $v1 = trim($v1);
      $correct = FALSE;
      if (!empty($v1) && $v1{0} == '#') {
        // Correct answer is marked with # symbol.
        $correct = TRUE;
        $v1 = trim(drupal_substr($v1, 1));
        $c++;
      }
      if ($v1) {
        $elements[$k]['options'][$i][0] = $v1;
        $elements[$k]['options'][$i][1] = $correct;
      }
      $i++;
    }
    if ($c != 1) {
      $message[$k] = t('You have to mark ONE correct answer in the Selector') . ' ' . $k;
    }
  }

  $options['description'] = $description;
  $options['elements'] = $elements;
  $options['message'] = $message;

}

/**
 * Submit handler for the "Next step" button.
 */
function langtests_ddl_next(&$form, &$form_state) {
  $form_state['stored_task'] = $form_state['values']['fieldset']['task'];
  $form_state['step'] = 2;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Previous step" button.
 */
function langtests_ddl_prev($form, &$form_state) {
  if (isset($form_state['values']['fieldset']['options'])) {
    foreach ($form_state['values']['fieldset']['options'] as $k => $v) {
      $form_state['stored_options'][$k] = $v;
    }
  }
  $form_state['step'] = 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $format = (isset($elements['format'])) ? $elements['format'] : filter_fallback_format();
  $next_tid = $options['next_tid'];

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  if (!empty($elements)) {
    $correct_answers = array();

    $i = 0; $n = 0;
    foreach ($elements as $i => $val) {
      if (is_numeric($i)) {
        // The number of select elements.
        $n++;
        $name = 'select_option_' . $i;
        $style = isset($form_state['stored_answers'][$i]) ? $form_state['stored_answers'][$i] : NULL;

        if (!is_null($style)) {
          $style = ($style) ? 'correct' : 'mistake';
        }

        $prefix = '';
        $newline = FALSE;
        if (isset($val['prefix'])) {
          // If the string starts with newline symbol (\r or \n)...
          if (strspn($val['prefix'], "\r\n")) {
            $newline = TRUE;
          }

          $prefix = check_markup($val['prefix'], $format);
          // Remove unnecessary <p> tags from the beginning...
          if (strpos($prefix, '<p>') === 0) {
            $prefix = drupal_substr($prefix, 3);
          }
          // ...and the end of the string...
          if (strrpos($prefix, '</p>') == (drupal_strlen($prefix) - 5)) {
            $prefix = drupal_substr($prefix, 0, -5);
          }

          // ...add a <br /> tag at the beginning.
          if ($newline) {
            $prefix = '<br />' . $prefix;
          }
        }
        $suffix = '';
        $newline = FALSE;
        if (isset($val['suffix'])) {
          // If the string starts with newline symbol (\r or \n)...
          if (strspn($val['suffix'], "\r\n")) {
            $newline = TRUE;
          }

          $suffix = check_markup($val['suffix'], $format);
          // Remove unnecessary <p> tags from the beginning...
          if (strpos($suffix, '<p>') === 0) {
            $suffix = drupal_substr($suffix, 3);
          }
          // ...and the end of the string...
          if (strrpos($suffix, '</p>') == (drupal_strlen($suffix) - 5)) {
            $suffix = drupal_substr($suffix, 0, -5);
          }

          // ...add a <br /> tag at the beginning.
          if ($newline) {
            $suffix = '<br />' . $suffix;
          }
        }

        $form[$name] = array(
          '#type' => 'select',
          '#prefix' => $prefix,
          '#suffix' => $suffix,
          '#empty_option' => t('Choose'),
          '#attributes' => !is_null($style) ? array('class' => array($style)) : '',
          // Non-standard property, but needed for theming.
          '#inline' => TRUE,
        );

        $j = 0;
        foreach ($val['options'] as $option) {
          // Add options.
          $form[$name]['#options'][] = $option[0];
          // Make array of correct answers.
          if ($option[1]) {
            $correct_answers[$i][] = $j;
          }
          $j++;
        }
      }
    }

    // Send parameters to _submit function.
    $form['elements_number'] = array('#type' => 'value', '#value' => $n);
    $form['answers'] = array('#type' => 'value', '#value' => $correct_answers);

    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['change_button'])) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
          '#prefix' => '<br /><br />',
        );
      }
      else {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#prefix' => '<br /><br />',
          '#submit' => $submit,
        );
      }
    }
  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $input = array();
  $result = 0;
  $ok = 0;

  for ($i = 1; $i <= $form_state['values']['elements_number']; $i++) {
    $name = 'select_option_' . $i;
    $correct = FALSE;

    $fi = isset($form_state['values'][$name]) ? $form_state['values'][$name] : NULL;
    $input[$i] = $fi;

    // Check answers.
    foreach ($form_state['values']['answers'][$i] as $answer) {
      if (!is_null($fi) && $fi != '' && $fi == $answer) {
        $correct = TRUE;
        $ok++;
        break;
      };
    }

    // Save information about correct and incorrect answers.
    $form_state['stored_answers'][$i] = $correct;
  }

  if ($ok == $form_state['values']['elements_number']) {
    $message = t('Congratulations! All your answers are correct!');
    $type = 'status';
    $result = 1;
  }
  else {
    $message = t('You scored !ok out of !total.',
                  array(
                    '!ok' => $ok,
                    '!total' => $form_state['values']['elements_number'],
                  )
                );
    $type = 'error';
  }

  $form_state['change_button'] = TRUE;

  $testresult['result'] = $result;
  $testresult['input'] = $input;

  drupal_set_message(check_plain($message), $type);
}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $format = isset($elements['format']) ? $elements['format'] : filter_fallback_format();
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  $correct_answers = array();

  $i = 0; $n = 0;
  foreach ($elements as $i => $val) {
    if (is_numeric($i)) {
      // The number of select elements.
      $n++;
      $name = 'select_option_' . $i;

      // Make an array of correct answers.
      $j = 0;
      foreach ($val['options'] as $option) {
        if ($option[1]) {
          $correct_answers[$i] = $j;
        }
        $j++;
      }

      $style = NULL;
      if (isset($input[$i]) && isset($correct_answers[$i])) {
        $style = ($input[$i] == $correct_answers[$i]) ? 'correct' : 'mistake';
      }

      $prefix = '';
      $newline = FALSE;
      if (isset($val['prefix'])) {
        // If the string starts with newline symbol (\r or \n)...
        if (strspn($val['prefix'], "\r\n")) {
          $newline = TRUE;
        }

        $prefix = check_markup($val['prefix'], $format);
        // Remove unnecessary <p> tags from the beginning...
        if (strpos($prefix, '<p>') === 0) {
          $prefix = drupal_substr($prefix, 3);
        }
        // ...and the end of the string...
        if (strrpos($prefix, '</p>') == (drupal_strlen($prefix) - 5)) {
          $prefix = drupal_substr($prefix, 0, -5);
        }

        // ...add a <br /> tag at the beginning.
        if ($newline) {
          $prefix = '<br />' . $prefix;
        }
      }
      $suffix = '';
      $newline = FALSE;
      if (isset($val['suffix'])) {
        // If the string starts with newline symbol (\r or \n)...
        if (strspn($val['suffix'], "\r\n")) {
          $newline = TRUE;
        }

        $suffix = check_markup($val['suffix'], $format);
        // Remove unnecessary <p> tags from the beginning...
        if (strpos($suffix, '<p>') === 0) {
          $suffix = drupal_substr($suffix, 3);
        }
        // ...and the end of the string...
        if (strrpos($suffix, '</p>') == (drupal_strlen($suffix) - 5)) {
          $suffix = drupal_substr($suffix, 0, -5);
        }

        // ...add a <br /> tag at the beginning.
        if ($newline) {
          $suffix = '<br />' . $suffix;
        }
      }

      $form[$name] = array(
        '#type' => 'select',
        '#prefix' => $prefix,
        '#suffix' => $suffix,
        '#empty_option' => t('Choose'),
        '#default_value' => isset($input[$i]) ? $input[$i] : NULL,
        '#attributes' => !is_null($style) ? array('class' => array($style)) : '',
        // Non-standard property, but needed for theming.
        '#inline' => TRUE,
      );

      // Add options.
      foreach ($val['options'] as $option) {
        $form[$name]['#options'][] = $option[0];
      }
    }
  }
}
