<?php
/**
 * @file
 * Functions for tests with type "Crossword"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  $elements = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $sid = $options['sid'];

  if (empty($form_state['step']) && (!isset($options['attributes']['size_x']) || !isset($options['attributes']['size_y']))) {
    $form_state['step'] = 1;
    $step = 1;
  }
  elseif (empty($form_state['step']) && isset($options['attributes']['size_x']) && isset($options['attributes']['size_y'])) {
    $form_state['stored_values']['size_x'] = $options['attributes']['size_x'];
    $form_state['stored_values']['size_y'] = $options['attributes']['size_y'];
    $form_state['step'] = 2;
    $step = 2;
  }
  else {
    $step = $form_state['step'];
  }

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  $title = '';
  if ($step == 1) {
    $title = t('Enter the dimensions of the crossword grid');
  }
  elseif ($step == 2) {
    $title = t('Enter the words across and down');
  }
  elseif ($step == 3) {
    $title = t('Enter the descriptions for words across and down');
  }

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
    '#title' => $title,
  );

  if ($step == 1) {
    $form['fieldset']['size_x'] = array(
      '#type' => 'select',
      '#title' => t('Width'),
      '#default_value' => 11,
      '#multiple' => FALSE,
      '#options' => array(
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
        12 => 12,
        13 => 13,
        14 => 14,
        15 => 15,
        16 => 16,
      ),
      '#required' => FALSE,
      '#description' => t('Select the horizontal extent of the crossword grid.'),
    );
    $form['fieldset']['size_y'] = array(
      '#type' => 'select',
      '#title' => t('Height'),
      '#default_value' => 11,
      '#multiple' => FALSE,
      '#options' => array(
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
        12 => 12,
        13 => 13,
        14 => 14,
        15 => 15,
        16 => 16,
      ),
      '#required' => FALSE,
      '#description' => t('Select the vertical extent of the crossword grid.'),
    );
  }
  elseif ($step == 2) {
    $size_x = (isset($options['attributes']['size_x'])) ? $options['attributes']['size_x'] : 11;
    $size_y = (isset($options['attributes']['size_y'])) ? $options['attributes']['size_y'] : 11;

    if (isset($form_state['values']['fieldset']['size_x'])) {
      $size_x = $form_state['values']['fieldset']['size_x'];
    }
    if (isset($form_state['values']['fieldset']['size_y'])) {
      $size_y = $form_state['values']['fieldset']['size_y'];
    }

    if (isset($form_state['stored_values']['size_x'])) {
      $size_x = $form_state['stored_values']['size_x'];
    }
    if (isset($form_state['stored_values']['size_y'])) {
      $size_y = $form_state['stored_values']['size_y'];
    }

    $form_state['stored_values']['size_x'] = $size_x;
    $form_state['stored_values']['size_y'] = $size_y;

    $form['fieldset']['cw_grid1'] = array(
      '#markup' => '<div style="width:100px;"><table class="crossword">',
    );

    // Grid here.
    $i = 0; $j = 0; $z = 0;
    for ($i = 0; $i < $size_y; $i++) {
      $form['fieldset']['cw_grid2-' . $i] = array(
        '#markup' => '<tr class="crossword-tr">',
      );
      for ($j = 0; $j < $size_x; $j++) {
        $form['fieldset']['cw_grid3-' . $z] = array(
          '#markup' => '<td id="' . $z . '" abbr="' . $j . '" class="crossword-td crossword-field">',
        );

        $default_value = (isset($correct_answers[$z])) ? $correct_answers[$z] : '';

        $form['fieldset'][$z] = array(
          '#type' => 'textfield',
          '#size' => 1,
          '#maxlength' => 1,
          '#attributes' => array('class' => array('crossword-form-text')),
          '#default_value' => $default_value,
        );

        $form['fieldset']['cw_grid4-' . $z] = array(
          '#markup' => '</td>',
        );
        $z++;
      }
      $form['fieldset']['cw_grid5-' . $i] = array(
        '#markup' => '</tr>',
      );
    }

    $form['fieldset']['cw_grid6'] = array(
      '#markup' => '</table></div><br />',
    );

  }
  elseif ($step == 3) {
    $size_x = $form_state['stored_values']['size_x'];
    $size_y = $form_state['stored_values']['size_y'];

    $form['fieldset']['cw_grid1'] = array(
      '#markup' => '<div style="width:100px;"><table class="crossword">',
    );

    // Grid here.
    $i = 0; $j = 0; $z = 0; $p = 1;
    for ($i = 0; $i < $size_y; $i++) {
      $form['fieldset']['cw_grid2-' . $i] = array(
        '#markup' => '<tr class="crossword-tr">',
      );
      for ($j = 0; $j < $size_x; $j++) {
        $class = (!empty($form_state['values']['fieldset'][$z]) && $form_state['values']['fieldset'][$z] != ' ') ? 'crossword-field' : '';
        $form['fieldset']['cw_grid3-' . $z] = array(
          '#markup' => '<td id="' . $z . '" abbr="' . $j . '" class="crossword-td ' . $class . '">',
        );

        // Actually, if there is a letter in this cell.
        if ($class) {

          // Make pointers for across/down words.
          $prefix = '';
          $classr = (!empty($form_state['values']['fieldset'][$z + 1]) && $form_state['values']['fieldset'][$z + 1] != ' ') ? TRUE : FALSE;
          $classl = (!empty($form_state['values']['fieldset'][$z - 1]) && $form_state['values']['fieldset'][$z - 1] != ' ') ? TRUE : FALSE;
          $classd = (!empty($form_state['values']['fieldset'][$z + $size_x]) && $form_state['values']['fieldset'][$z + $size_x] != ' ') ? TRUE : FALSE;
          $classu = (!empty($form_state['values']['fieldset'][$z - $size_x]) && $form_state['values']['fieldset'][$z - $size_x] != ' ') ? TRUE : FALSE;

          if (($classr && (!$classl || $j == 0) && $j < $size_x - 1)
              || ($classd && !$classu)) {
            $prefix = '<div class="crossword-pointer">' . $p . '</div>';
            $p++;
          }

          $form['fieldset'][$z] = array(
            '#type' => 'textfield',
            '#size' => 1,
            '#maxlength' => 1,
            '#prefix' => $prefix,
            '#attributes' => array('class' => array('crossword-form-text')),
          );
        }
        else {
          $form['fieldset']['cw_grid3a-' . $z] = array(
            '#markup' => '<div style="width:30px;">&nbsp;</div>',
          );
        }

        $form['fieldset']['cw_grid4-' . $z] = array(
          '#markup' => '</td>',
        );
        $z++;
      }
      $form['fieldset']['cw_grid5-' . $i] = array(
        '#markup' => '</tr>',
      );
    }

    $form['fieldset']['cw_grid6'] = array(
      '#markup' => '</table></div><br />',
    );

    // Across/Down description.
    $default_value = '';
    if (isset($form_state['stored_values']['across']['value'])) {
      $default_value = $form_state['stored_values']['across']['value'];
    }
    elseif (isset($elements['across']['value'])) {
      $default_value = $elements['across']['value'];
    }
    $format = NULL;
    if (isset($form_state['stored_values']['across']['format'])) {
      $format = $form_state['stored_values']['across']['format'];
    }
    elseif (isset($elements['across']['format'])) {
      $format = $elements['across']['format'];
    }
    $form['fieldset']['across'] = array(
      '#type' => 'text_format',
      '#title' => t('Across'),
      '#description' => t('Enter descriptions for words across.'),
      '#default_value' => $default_value,
      '#format' => $format,
    );

    $default_value = '';
    if (isset($form_state['stored_values']['down']['value'])) {
      $default_value = $form_state['stored_values']['down']['value'];
    }
    elseif (isset($elements['down']['value'])) {
      $default_value = $elements['down']['value'];
    }
    $format = NULL;
    if (isset($form_state['stored_values']['down']['format'])) {
      $format = $form_state['stored_values']['down']['format'];
    }
    elseif (isset($elements['down']['format'])) {
      $format = $elements['down']['format'];
    }
    $form['fieldset']['down'] = array(
      '#type' => 'text_format',
      '#title' => t('Down'),
      '#description' => t('Enter descriptions for words down.'),
      '#default_value' => $default_value,
      '#format' => $format,
    );

    $form['fieldset']['cw_grid7'] = array(
      '#markup' => '</td></tr></table><br />',
    );
  }

  if ($step == 1 || $step == 2) {
    // Next step button.
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#submit' => array('langtests_crw_next'),
    );
  }
  elseif ($step == 3) {
    // Previous step button.
    $form['prev'] = array(
      '#type' => 'submit',
      '#value' => t('Previous step'),
      '#submit' => array('langtests_crw_prev'),
    );
    // Submit button.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }

  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );

  // Add javascript stuff.
  if ($step == 2 || $step == 3) {

    $settings = array(
      'submitted' => 0,
      'size_x' => $size_x,
      'input_id_name' => 'input#edit-fieldset-',
    );

    drupal_add_js(array('langtests' => $settings), 'setting');
    drupal_add_js(drupal_get_path('module', 'langtests') . '/includes/testtype.crw.js', 'file');

  }
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];

  // Make attributes array.
  $attributes = array();
  $attributes['size_x'] = $form_state['stored_values']['size_x'];
  $attributes['size_y'] = $form_state['stored_values']['size_y'];

  // Make elements array.
  $elements = array();
  $elements['across']['value'] = $form_state['values']['fieldset']['across']['value'];
  $elements['across']['format'] = $form_state['values']['fieldset']['across']['format'];
  $elements['down']['value'] = $form_state['values']['fieldset']['down']['value'];
  $elements['down']['format'] = $form_state['values']['fieldset']['down']['format'];

  // Make correct_answers array.
  $correct_answers = array();
  foreach ($form_state['values']['fieldset'] as $k => $v) {
    if (is_numeric($k)) {
      $correct_answers[$k] = mb_strtoupper(mb_substr($v, 0, 1));
    }
  }

  $options['description'] = $description;
  $options['elements'] = $elements;
  $options['attributes'] = $attributes;
  $options['correct_answers'] = $correct_answers;

}

/**
 * Submit handler for the "Next step" button.
 */
function langtests_crw_next(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step++;
  if (isset($form_state['values']['fieldset']['size_x']) && isset($form_state['values']['fieldset']['size_y'])) {
    $form_state['stored_values']['size_x'] = $form_state['values']['fieldset']['size_x'];
    $form_state['stored_values']['size_y'] = $form_state['values']['fieldset']['size_y'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Previous step" button.
 */
function langtests_crw_prev(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step--;
  $form_state['stored_values']['across']['value'] = $form_state['values']['fieldset']['across']['value'];
  $form_state['stored_values']['across']['format'] = $form_state['values']['fieldset']['across']['format'];
  $form_state['stored_values']['down']['value'] = $form_state['values']['fieldset']['down']['value'];
  $form_state['stored_values']['down']['format'] = $form_state['values']['fieldset']['down']['format'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $acrossdown = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $size_x = isset($options['attributes']['size_x']) ? $options['attributes']['size_x'] : 11;
  $size_y = isset($options['attributes']['size_y']) ? $options['attributes']['size_y'] : 11;
  $next_tid = $options['next_tid'];

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  if (!empty($correct_answers)) {
    $fields = array_keys($correct_answers);

    $form['cw_grid1'] = array(
      '#markup' => '<table><tr><td valign="top"><table class="crossword">',
    );

    // Grid cells here.
    $i = 0; $j = 0; $z = 0; $p = 1;
    for ($i = 0; $i < $size_y; $i++) {
      $form['cw_grid2-' . $i] = array(
        '#markup' => '<tr class="crossword-tr">',
      );
      for ($j = 0; $j < $size_x; $j++) {
        $class_td = in_array($z, $fields) ? 'crossword-field' : '';
        $form['cw_grid3-' . $z] = array(
          '#markup' => '<td id="' . $z . '" abbr="' . $j . '" class="crossword-td ' . $class_td . '">',
        );
        if (in_array($z, $fields)) {
          $name = 'textfield_' . $z;
          $style = isset($form_state['stored_answers'][$name]) ? $form_state['stored_answers'][$name] : NULL;

          if (!is_null($style)) {
            $style = ($style) ? 'crossword-correct' : 'crossword-mistake';
          }

          // Make pointers for across/down words.
          $prefix = '';
          if ((in_array($z + 1, $fields) && (!in_array($z - 1, $fields) || $j == 0) && $j < $size_x - 1)
              || (in_array($z + $size_x, $fields) && !in_array($z - $size_x, $fields))) {
            $prefix = '<div class="crossword-pointer">' . $p . '</div>';
            $p++;
          }

          $form[$name] = array(
            '#type' => 'textfield',
            '#size' => 1,
            '#maxlength' => 1,
            '#prefix' => $prefix,
            '#attributes' => array('class' => array('crossword-form-text', $style)),
          );
        }
        else {
          $form['cw_grid4-' . $z] = array(
            '#markup' => '&nbsp;',
          );
        }
        $form['cw_grid5-' . $z] = array(
          '#markup' => '</td>',
        );
        $z++;
      }
      $form['cw_grid6' . $i] = array(
        '#markup' => '</tr>',
      );
    }

    $form['cw_grid7'] = array(
      '#markup' => '</table></td><td valign="top">',
    );

    // Across/Down description.
    $markup = '<strong>' . t('Across') . '</strong>';
    $markup .= check_markup($acrossdown['across']['value'], $acrossdown['across']['format']);
    $markup .= '<strong>' . t('Down') . '</strong>';
    $markup .= check_markup($acrossdown['down']['value'], $acrossdown['down']['format']);

    $form['acrossdown'] = array(
      '#markup' => $markup,
    );

    $form['js_warning'] = array(
      '#markup' => '<br /><br /><div id="js_warning" class="description">' . t('It appears that your browser has JavaScript disabled. Please consider enabling JavaScript for your greater convenience.') . '</div>',
    );

    $form['cw_grid8'] = array(
      '#markup' => '</td></tr></table>',
    );

    // Send parameters to _submit function.
    $form['correct_answers'] = array('#type' => 'value', '#value' => $correct_answers);

    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['change_button'])) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
          '#prefix' => '<br /><br />',
        );
      }
      else {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#prefix' => '<br /><br />',
          '#submit' => $submit,
        );
      }
    }

    // Add javascript stuff.
    $submitted = isset($form_state['change_button']) ? 1 : 0;

    $settings = array(
      'submitted' => $submitted,
      'size_x' => $size_x,
      'input_id_name' => 'input#edit-textfield-',
    );

    drupal_add_js(array('langtests' => $settings), 'setting');
    drupal_add_js(drupal_get_path('module', 'langtests') . '/includes/testtype.crw.js', 'file');

  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $input = array();
  $result = 0;
  $ok = 0;

  foreach ($form_state['values']['correct_answers'] as $k => $v) {
    $name = 'textfield_' . $k;
    $correct = FALSE;

    $fk = isset($form_state['values'][$name]) ? mb_strtoupper(mb_substr($form_state['values'][$name], 0, 1)) : NULL;
    $input[$k] = $fk;

    if (!is_null($fk) && $fk != '' && $fk == $v) {
      $correct = TRUE;
      $ok++;
    }

    $form_state['stored_answers'][$name] = $correct;
  }

  if ($ok == count($form_state['values']['correct_answers'])) {
    $message = t('Congratulations! All your answers are correct!');
    $type = 'status';
    $result = 1;
  }
  else {
    $message = t("You've made some mistakes.");
    $type = 'error';
  }

  drupal_set_message(check_plain($message), $type);

  $form_state['change_button'] = TRUE;

  $testresult['result'] = $result;
  $testresult['input'] = $input;

}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $acrossdown = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $size_x = isset($options['attributes']['size_x']) ? $options['attributes']['size_x'] : 11;
  $size_y = isset($options['attributes']['size_y']) ? $options['attributes']['size_y'] : 11;
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  $fields = array_keys($correct_answers);

  $form['cw_grid1'] = array(
    '#markup' => '<table><tr><td valign="top"><table class="crossword">',
  );

  // Grid cells here.
  $i = 0; $j = 0; $z = 0; $p = 1;
  for ($i = 0; $i < $size_y; $i++) {
    $form['cw_grid2-' . $i] = array(
      '#markup' => '<tr class="crossword-tr">',
    );
    for ($j = 0; $j < $size_x; $j++) {
      $class_td = in_array($z, $fields) ? 'crossword-field' : '';
      $form['cw_grid3-' . $z] = array(
        '#markup' => '<td id="' . $z . '" abbr="' . $j . '" class="crossword-td ' . $class_td . '">',
      );
      if (in_array($z, $fields)) {
        $name = 'textfield_' . $z;
        $style = NULL;
        if (isset($input[$z]) && isset($correct_answers[$z])) {
          $style = ($input[$z] == $correct_answers[$z]) ? 'crossword-correct' : 'crossword-mistake';
        }

        // Make pointers for across/down words.
        $prefix = '';
        if ((in_array($z + 1, $fields) && (!in_array($z - 1, $fields) || $j == 0) && $j < $size_x - 1)
            || (in_array($z + $size_x, $fields) && !in_array($z - $size_x, $fields))) {
          $prefix = '<div class="crossword-pointer">' . $p . '</div>';
          $p++;
        }

        $form[$name] = array(
          '#type' => 'textfield',
          '#size' => 1,
          '#maxlength' => 1,
          '#prefix' => $prefix,
          '#default_value' => isset($input[$z]) ? $input[$z] : NULL,
          '#attributes' => array('class' => array('crossword-form-text', $style)),
        );
      }
      else {
        $form['cw_grid4-' . $z] = array(
          '#markup' => '&nbsp;',
        );
      }
      $form['cw_grid5-' . $z] = array(
        '#markup' => '</td>',
      );
      $z++;
    }
    $form['cw_grid6' . $i] = array(
      '#markup' => '</tr>',
    );
  }

  $form['cw_grid7'] = array(
    '#markup' => '</table></td><td valign="top">',
  );

  // Across/Down description.
  $markup = '<strong>' . t('Across') . '</strong>';
  $markup .= check_markup($acrossdown['across']['value'], $acrossdown['across']['format']);
  $markup .= '<strong>' . t('Down') . '</strong>';
  $markup .= check_markup($acrossdown['down']['value'], $acrossdown['down']['format']);

  $form['acrossdown'] = array(
    '#markup' => $markup,
  );

  $form['cw_grid8'] = array(
    '#markup' => '</td></tr></table>',
  );
}
