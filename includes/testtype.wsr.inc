<?php
/**
 * @file
 * Functions for tests with type "Wordsearch"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  $elements = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $sid = $options['sid'];

  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = $form_state['step'];
  if (is_array($elements) && isset($elements[0])) {
    $step = 2;
  }

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
    '#title' => ($step == 1) ? t('Enter the dimensions of the wordsearch grid') : t('You can not continue without JavaScript. Please consider enabling JavaScript in your browser.'),
  );

  if ($step == 1) {
    $form['fieldset']['size_x'] = array(
      '#type' => 'select',
      '#title' => t('Width'),
      '#default_value' => 11,
      '#multiple' => FALSE,
      '#options' => array(
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
        12 => 12,
        13 => 13,
        14 => 14,
        15 => 15,
        16 => 16,
      ),
      '#required' => FALSE,
      '#description' => t('Select the horizontal extent of the wordsearch grid.'),
    );
    $form['fieldset']['size_y'] = array(
      '#type' => 'select',
      '#title' => t('Height'),
      '#default_value' => 11,
      '#multiple' => FALSE,
      '#options' => array(
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
        10 => 10,
        11 => 11,
        12 => 12,
        13 => 13,
        14 => 14,
        15 => 15,
        16 => 16,
      ),
      '#required' => FALSE,
      '#description' => t('Select the vertical extent of the wordsearch grid.'),
    );
  }
  elseif ($step == 2) {

    $size_x = (isset($options['attributes']['size_x'])) ? $options['attributes']['size_x'] : 11;
    $size_y = (isset($options['attributes']['size_y'])) ? $options['attributes']['size_y'] : 11;

    if (isset($form_state['values']['fieldset']['size_x'])) {
      $size_x = $form_state['values']['fieldset']['size_x'];
    }
    if (isset($form_state['values']['fieldset']['size_y'])) {
      $size_y = $form_state['values']['fieldset']['size_y'];
    }

    $form_state['stored_size']['size_x'] = $size_x;
    $form_state['stored_size']['size_y'] = $size_y;

    $active_cells = array();
    if (is_array($correct_answers) && count($correct_answers) > 0) {
      $l = implode(' ', $correct_answers);
      $active_cells = explode(' ', $l);
    }

    $form['fieldset']['ws_grid1'] = array(
      '#markup' => '<table><tr><td valign="top"><table class="wordsearch">',
    );

    // Grid here.
    $i = 0; $j = 0; $z = 0;
    for ($i = 0; $i < $size_y; $i++) {
      $form['fieldset']['ws_grid2-' . $i] = array(
        '#markup' => '<tr class="wordsearch-tr">',
      );
      for ($j = 0; $j < $size_x; $j++) {
        $class = (in_array($z, $active_cells)) ? 'wordsearch-admin-over' : 'wordsearch-admin-out';
        $form['fieldset']['ws_grid3-' . $z] = array(
          '#markup' => '<td id="' . $z . '" abbr="' . $j . '" class="wordsearch-admin-td ' . $class . '">',
        );

        $default_value = (isset($elements[$z])) ? $elements[$z] : '';

        $form['fieldset'][$z] = array(
          '#type' => 'textfield',
          '#size' => 1,
          '#maxlength' => 1,
          '#attributes' => array('class' => array('wordsearch-form-text'), 'readonly' => 'readonly'),
          '#default_value' => $default_value,
        );

        $form['fieldset']['ws_grid4-' . $z] = array(
          '#markup' => '</td>',
        );
        $z++;
      }
      $form['fieldset']['ws_grid5-' . $i] = array(
        '#markup' => '</tr>',
      );
    }

    $form['fieldset']['ws_grid6'] = array(
      '#markup' => '</table></td><td valign="top">',
    );

    // Place for answers.
    $answers = '';
    if (is_array($correct_answers)) {
      foreach ($correct_answers as $v) {
        $vm = explode(' ', trim($v));
        foreach ($vm as $k) {
          $answers .= (isset($elements[$k]) && !empty($elements[$k])) ? $elements[$k] : '*';
        }
        $answers .= '<br />';
      }
    }
    $noanswer = ($answers) ? '' : t('You have not entered any word');

    $fset_markup = '<div id="answers-topic" class="wordsearch-answers"><h2>' . t('Answers') . '</h2>
                    <div id="answer-no">' . $noanswer . '</div>
                    <div id="answer-words" style = "width: 200px; overflow-x: auto;">' . $answers . '</div><br />
                    <input type="button" id="clear-button" value="' . t('Clear') . '" class="form-submit"></div>';
    $form['fieldset']['answer_words'] = array('#markup' => $fset_markup);

    $form['fieldset']['answers'] = array(
      '#type' => 'textarea',
      '#rows' => 1,
      '#attributes' => array('readonly' => 'readonly', 'style' => 'display: none;'),
      '#resizable' => FALSE,
      '#default_value' => is_array($correct_answers) ? implode("\n", $correct_answers) . "\n" : '',
    );

    $form['fieldset']['ws_grid7'] = array(
      '#markup' => '</td></tr></table><br />',
    );
  }

  if ($step == 1) {
    // Next step button.
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#submit' => array('langtests_wsr_next'),
    );
  }
  elseif ($step == 2) {
    // Submit button.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );

  // Add javascript stuff.
  if ($step == 2) {
    drupal_add_library('system', 'ui.dialog');

    $settings = array(
      'enter_letters_message' => t('Enter letters, then highlight the words with the mouse'),
      'accept_the_word_message' => t('Accept the word'),
      'wanna_this_word_message' => t('Do you want to accept this word?'),
      'yes' => t('Yes'),
      'no' => t('No'),
    );

    drupal_add_js(array('langtests' => $settings), 'setting');
    drupal_add_js(drupal_get_path('module', 'langtests') . '/includes/testtype.wsr.a.js', 'file');
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];

  // Make attributes array.
  $attributes = array();
  $attributes['size_x'] = $form_state['stored_size']['size_x'];
  $attributes['size_y'] = $form_state['stored_size']['size_y'];

  // Make elements array.
  $elements = array();
  foreach ($form_state['values']['fieldset'] as $k => $v) {
    if (is_numeric($k)) {
      $elements[$k] = mb_strtoupper($v);
    }
  }

  // Make correct_answers array.
  $correct_answers = array();
  $cans = trim($form_state['values']['fieldset']['answers']);
  if ($cans) {
    $correct_answers  = explode("\n", str_replace(array("\r\n", "\n\r"), "\n", $cans));
  }
  $correct_answers = array_unique($correct_answers);

  $options['description'] = $description;
  $options['elements'] = $elements;
  $options['attributes'] = $attributes;
  $options['correct_answers'] = $correct_answers;

}

/**
 * Submit handler for the "Next step" button.
 */
function langtests_wsr_next(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step = 2;
  $form_state['stored_size']['size_x'] = $form_state['values']['fieldset']['size_x'];
  $form_state['stored_size']['size_y'] = $form_state['values']['fieldset']['size_y'];
  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $letters = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $size_x = isset($options['attributes']['size_x']) ? $options['attributes']['size_x'] : 11;
  $size_y = isset($options['attributes']['size_y']) ? $options['attributes']['size_y'] : 11;
  $next_tid = $options['next_tid'];

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  if (!empty($correct_answers)) {
    // Make an array of correct words.
    $correct_words = array();
    foreach ($correct_answers as $v) {
      $wordcode = explode(' ', $v);
      // Will assemble letters of the word here.
      $cword = '';
      foreach ($wordcode as $w) {
        $cword .= $letters[$w];
      }
      $correct_words[] = $cword;
    }

    $count_answers = count($correct_answers);

    $active_cells = array();
    $l = '';
    foreach ($correct_answers as $v) {
      $l .= $v . ' ';
    }
    $l = trim($l);
    $active_cells = explode(' ', $l);

    $ws_grid = '<table><tr><td valign="top">';

    // Grid here.
    $i = 0; $j = 0; $z = 0;
    $ws_grid .= '<table class="wordsearch">';
    for ($i = 0; $i < $size_y; $i++) {
      $ws_grid .= '<tr class="wordsearch-tr">';
      for ($j = 0; $j < $size_x; $j++) {
        $class_td = (in_array($z, $active_cells) && isset($form_state['change_button'])) ? 'wordsearch-over' : 'wordsearch-out';
        $ws_grid .= '<td id="' . $z . '"abbr = "' . $j . '" class="wordsearch-td ' . $class_td . '">' . $letters[$z] . '</td>';
        $z++;
      }
      $ws_grid .= '</tr>';
    }
    $ws_grid .= '</table></td><td valign="top">';

    $form['ws_grid1'] = array(
      '#markup' => $ws_grid,
    );

    // Place for answers.
    $form['wordsearch'] = array(
      '#type' => 'textarea',
      '#rows' => $count_answers + 1,
      '#title' => isset($form_state['change_button']) ? t('Correct answer') : t('Enter !count words here', array('!count' => $count_answers)),
      '#description' => isset($form_state['change_button']) ? NULL : t('It appears that your browser has JavaScript disabled, therefore you can only key in the answers. Please consider enabling JavaScript for your greater convenience.'),
      '#attributes' => isset($form_state['change_button']) ? array('readonly' => 'readonly') : NULL,
    );

    $ws_grid = '</td></tr></table><br />';

    $form['ws_grid2'] = array(
      '#markup' => $ws_grid,
    );

    // Send parameters to _submit function.
    $form['correct_words'] = array('#type' => 'value', '#value' => $correct_words);

    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['change_button'])) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
          '#prefix' => '<br /><br />',
        );
      }
      else {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#prefix' => '<br /><br />',
          '#submit' => $submit,
        );
      }
    }

    // Add javascript stuff.
    if (!isset($form_state['change_button'])) {

      $settings = array(
        'count_answers' => $count_answers,
        'words_left' => t('Words left:'),
        'highlight_words_message' => t('Highlight the words with the mouse.'),
        'correct_answers_json' => json_encode($correct_answers, JSON_FORCE_OBJECT),
        'press_check_message' => t('Press Check now!'),
      );

      drupal_add_js(array('langtests' => $settings), 'setting');
      drupal_add_js(drupal_get_path('module', 'langtests') . '/includes/testtype.wsr.b.js', 'file');

    }
  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $answers = array();
  $correct_words = array();
  $input = array();
  $result = 0;
  $ok = 0;

  // Make array of words from answer.
  $answer = mb_strtoupper(check_plain($form_state['values']['wordsearch']));
  $answers = preg_split("/[\s,;]+/", $answer, -1, PREG_SPLIT_NO_EMPTY);
  $input = $answers;

  // Restore array of correct words.
  $correct_words = $form_state['values']['correct_words'];

  // Remove correct answers from both arrays.
  foreach ($answers as $k => $v) {
    if (in_array($v, $correct_words)) {
      unset($correct_words[array_search($v, $correct_words)]);
      unset($answers[$k]);
    }
  }

  if (count($correct_words) > 0) {
    $message = t("You've missed some of the words:") . ' ';
    foreach ($correct_words as $v) {
      $message .= $v . ' ';
    }
    drupal_set_message(check_plain($message), 'error');
  }

  if (count($answers) > 0) {
    $message = t("You've entered some excess words:") . ' ';
    foreach ($answers as $v) {
      $message .= $v . ' ';
    }
    drupal_set_message(check_plain($message), 'error');
  }

  if (count($correct_words) == 0 && count($answers) == 0) {
    $message = t('Congratulations! All your answers are correct!');
    drupal_set_message(check_plain($message), 'status');
    $result = 1;
  }

  $form_state['change_button'] = TRUE;

  $testresult['result'] = $result;
  $testresult['input'] = $input;

}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $letters = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $size_x = isset($options['attributes']['size_x']) ? $options['attributes']['size_x'] : 11;
  $size_y = isset($options['attributes']['size_y']) ? $options['attributes']['size_y'] : 11;
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  // Make an array of correct words.
  $correct_words = array();
  foreach ($correct_answers as $v) {
    $wordcode = explode(' ', $v);
    // Will assemble letters of the word here.
    $cword = '';
    foreach ($wordcode as $w) {
      $cword .= $letters[$w];
    }
    $correct_words[] = $cword;
  }

  $active_cells = array();
  $l = '';
  foreach ($correct_answers as $v) {
    $l .= $v . ' ';
  }
  $l = trim($l);
  $active_cells = explode(' ', $l);

  $ws_grid = '<table><tr><td valign="top">';

  // Grid here.
  $i = 0; $j = 0; $z = 0;
  $ws_grid .= '<table class="wordsearch">';
  for ($i = 0; $i < $size_y; $i++) {
    $ws_grid .= '<tr class="wordsearch-tr">';
    for ($j = 0; $j < $size_x; $j++) {
      $class_td = in_array($z, $active_cells) ? 'wordsearch-over' : 'wordsearch-out';
      $ws_grid .= '<td id="' . $z . '"abbr = "' . $j . '" class="wordsearch-td ' . $class_td . '">' . $letters[$z] . '</td>';
      $z++;
    }
    $ws_grid .= '</tr>';
  }
  $ws_grid .= '</table></td><td valign="top">';

  $form['ws_grid1'] = array(
    '#markup' => $ws_grid,
  );

  $input_b = $input;
  // Remove correct answers from both arrays.
  foreach ($input as $k => $v) {
    if (in_array($v, $correct_words)) {
      unset($correct_words[array_search($v, $correct_words)]);
      unset($input[$k]);
    }
  }

  $message = '';
  if (count($correct_words) > 0) {
    $message = '<font class = "mistake"><strong>' . t('Words missed:') . '</strong><br />';
    foreach ($correct_words as $v) {
      $message .= $v . '<br />';
    }
    $message .= '<br /></font>';
  }

  if (count($input) > 0) {
    $message .= '<font class = "mistake"><strong>' . t('Excess words:') . '</strong><br />';
    foreach ($input as $v) {
      $message .= $v . '<br />';
    }
    $message .= '<br /></font>';
  }

  if (count($correct_words) == 0 && count($input) == 0) {
    $message .= '<font class = "correct"><strong>' . t('Correct words:') . '</strong><br />';
    foreach ($input_b as $v) {
      $message .= $v . '<br />';
    }
    $message .= '<br /></font>';
  }

  // Place for result message.
  $form['answers'] = array(
    '#markup' => $message,
  );

  $ws_grid = '</td></tr></table><br />';

  $form['ws_grid2'] = array(
    '#markup' => $ws_grid,
  );

}
