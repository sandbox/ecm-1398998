<?php
/**
 * @file
 * Functions for tests with type "Matcher"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  $elements = $options['elements'];
  $sid = $options['sid'];

  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = $form_state['step'];

  if (isset($form_state['stored_values']['elements'])) {
    $elements = $form_state['stored_values']['elements'];
  }
  else {
    $form_state['stored_values']['elements'] = $elements;
  }

  $count = is_array($elements) ? (count($elements) / 2) : 0;
  $form_state['stored_values']['count'] = $count;

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
    '#title' => t('Enter pairs in the correct order'),
  );

  if ($count) {
    $form['fieldset']['markup_t11'] = array(
      '#markup' => '<table>',
    );
    for ($i = 1; $i <= $count; $i++) {
      $form['fieldset']['l' . $i] = array(
        '#type' => 'textfield',
        '#title' => ($i == 1) ? t('Left part') : '',
        '#default_value' => isset($elements['l' . $i]) ? $elements['l' . $i] : '',
        '#maxlength' => 256,
        '#prefix' => '<tr><td width="50%">',
        '#suffix' => '</td>',
      );
      $form['fieldset']['r' . $i] = array(
        '#type' => 'textfield',
        '#title' => ($i == 1) ? t('Right part') : '',
        '#default_value' => isset($elements['r' . $i]) ? $elements['r' . $i] : '',
        '#maxlength' => 256,
        '#prefix' => '<td width="50%">',
        '#suffix' => '</td></tr>',
      );
    }
    $form['fieldset']['markup_t12'] = array(
      '#markup' => '</table>',
    );
  }

  // Fields for entering extra pair.
  if ($count) {
    $form['fieldset']['markup2'] = array(
      '#markup' => '<br /><br />' . t('You can add one more pair here. Leave the fields blank if not required.') . '<br />',
    );
  }

  $form['fieldset']['markup_t1'] = array(
    '#markup' => '<table><tr>',
  );
  $form['fieldset']['l' . ($count + 1)] = array(
    '#type' => 'textfield',
    '#title' => t('Left part'),
    '#maxlength' => 256,
    '#description' => t('Enter the left part of the task element.'),
    '#prefix' => '<td width="50%">',
    '#suffix' => '</td>',
  );
  $form['fieldset']['r' . ($count + 1)] = array(
    '#type' => 'textfield',
    '#title' => t('Right part'),
    '#maxlength' => 256,
    '#description' => t('Enter the right part of the task element.'),
    '#prefix' => '<td width="50%">',
    '#suffix' => '</td>',
  );
  $form['fieldset']['markup_t2'] = array(
    '#markup' => '</tr></table>',
  );

  // Add a pair button.
  $form['fieldset']['next'] = array(
    '#type' => 'submit',
    '#value' => t('Add a pair'),
    '#submit' => array('langtests_mtr_next'),
  );
  // Delete a pair button.
  if ($count) {
    $form['fieldset']['prev'] = array(
      '#type' => 'submit',
      '#value' => t('Delete last pair'),
      '#submit' => array('langtests_mtr_prev'),
      '#limit_validation_errors' => array(),
    );
  }

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );
}

/**
 * Validate handler.
 */
function langtests_testtype_validator($form, &$form_state) {
  $count = $form_state['stored_values']['count'];

  for ($i = 1; $i <= $count; $i++) {
    if (trim($form_state['values']['fieldset']['l' . $i]) == '') {
      form_set_error('fieldset][l' . $i, t('You must fill in the field "Left part') . ' ' . $i . '"');
    }
    if (trim($form_state['values']['fieldset']['r' . $i]) == '') {
      form_set_error('fieldset][r' . $i, t('You must fill in the field "Right part') . ' ' . $i . '"');
    }
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];
  $count = $form_state['stored_values']['count'];

  $ret = 0;
  if ($count == 0 && trim($form_state['values']['fieldset']['l1']) == '') {
    form_set_error('fieldset][l1', t('You must fill in the field "Left part"'));
    $ret++;
  }
  if ($count == 0 && trim($form_state['values']['fieldset']['r1']) == '') {
    form_set_error('fieldset][exp1', t('You must fill in the field "Right part"'));
    $ret++;
  }
  if ($ret) {
    return;
  }

  // Make elements array.
  $elements = array();
  for ($i = 1; $i <= $count; $i++) {
    $li = isset($form_state['values']['fieldset']['l' . $i]) ? trim($form_state['values']['fieldset']['l' . $i]) : NULL;
    if ($li) {
      // Remove whitespaces.
      $li = preg_replace('~\s+~', ' ', $li);
    }
    $elements['l' . $i] = $li;

    $ri = isset($form_state['values']['fieldset']['r' . $i]) ? trim($form_state['values']['fieldset']['r' . $i]) : NULL;
    // Remove whitespaces.
    if (!is_null($ri)) {
      $ri = preg_replace('~\s+~', ' ', $ri);
    }

    $elements['r' . $i] = $ri;
  }

  $ln = isset($form_state['values']['fieldset']['l' . ($count + 1)]) ? trim($form_state['values']['fieldset']['l' . ($count + 1)]) : NULL;
  $rn = isset($form_state['values']['fieldset']['r' . ($count + 1)]) ? trim($form_state['values']['fieldset']['r' . ($count + 1)]) : NULL;
  if ($ln && $rn) {
    $elements['l' . ($count + 1)] = $ln;
    $elements['r' . ($count + 1)] = $rn;
  }

  $options['description'] = $description;
  $options['elements'] = $elements;

}

/**
 * Submit handler for the "Add a pair" button.
 */
function langtests_mtr_next(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step++;

  $count = $form_state['stored_values']['count'] + 1;

  if (trim($form_state['values']['fieldset']['l' . $count]) && trim($form_state['values']['fieldset']['r' . $count])) {
    $form_state['stored_values']['elements']['l' . $count] = $form_state['values']['fieldset']['l' . $count];
    $form_state['stored_values']['elements']['r' . $count] = $form_state['values']['fieldset']['r' . $count];
  }
  else {
    drupal_set_message(t('You must fill in both fields: "Left part" and "Right part"'), 'error');
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Delete last pair" button.
 */
function langtests_mtr_prev(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step--;

  $count = $form_state['stored_values']['count'];

  unset($form_state['stored_values']['elements']['l' . $count]);
  unset($form_state['stored_values']['elements']['r' . $count]);
  // Also clear new pair fields.
  unset($form_state['input']['fieldset']['l' . $count]);
  unset($form_state['input']['fieldset']['r' . $count]);

  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $next_tid = $options['next_tid'];
  $submitted = 0;
  $tid = $options['tid'];

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  if (!empty($elements)) {

    $left_part = array();
    $right_part = array();
    $n = count($elements) / 2;
    for ($i = 1; $i <= $n; $i++) {
      $left_part[$i] = $elements['l' . $i];
      $right_part['r' . $i] = $elements['r' . $i];
    }

    $keys = array();
    if (isset($_SESSION['keys' . $tid])) {
      $keys = $_SESSION['keys' . $tid];
    }
    else {
      $keys = array_keys($right_part);
      shuffle($keys);
      $_SESSION['keys' . $tid] = $keys;
    }

    $maup = '<div style="position: relative"><table class="matcher">';
    $form['table_start'] = array('#markup' => $maup);

    $correct_index = '';

    foreach ($left_part as $i => $val) {
      $name = 'f' . $i;
      $prefix = '<tr><td>' . $i . '</td><td class="matcher-source">' . $val . '</td><td class="rathtd" width="30%">&nbsp;</td><td class="matcher-hidden">';
      $suffix = '</td><td class="matcher-target">' . $right_part[$keys[$i - 1]] . '</td></tr>';

      $style = isset($form_state[$name]['correct']) ? $form_state[$name]['correct'] : NULL;

      $value = isset($form_state['values'][$name]) ? $form_state['values'][$name] : NULL;
      if ($style) {
        $correct_index .= $value . ',';
      }

      if (!is_null($style)) {
        $style = ($style) ? 'correct' : 'mistake';
      }

      $form[$name] = array(
        '#type' => 'textfield',
        '#size' => 1,
        '#maxlength' => 3,
        '#prefix' => $prefix,
        '#suffix' => $suffix,
        '#attributes' => array('class' => array('matcher-form-text', $style)),
      );
    }

    $form['table_stop'] = array(
      // Close table.
      '#markup' => '</table><div id="mtrholder" style="position: absolute;"></div></div>',
    );

    // Send parameters to _submit function.
    $form['elements_number'] = array('#type' => 'value', '#value' => $n);
    $form['keys'] = array('#type' => 'value', '#value' => $keys);

    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['change_button'])) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
          '#prefix' => '<br /><br />',
        );
      }
      else {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#prefix' => '<br /><br />',
          '#submit' => $submit,
        );

        if (isset($_SESSION['keys' . $tid])) {
          unset($_SESSION['keys' . $tid]);
        }
        $submitted = 1;

      }
    }

    // Add Raphael Library.
    if ($raphpath = libraries_get_path('raphael')) {
      drupal_add_js($raphpath . '/raphael-min.js');
    }

    // Add javascript stuff.
    $right_keys = '';
    foreach ($keys as $k) {
      // Remove r from key name.
      $right_keys .= drupal_substr($k, 1) . ',';
    }

    $settings = array(
      'submitted' => $submitted,
      'correct_index' => drupal_substr($correct_index, 0, -1),
      'right_keys' => drupal_substr($right_keys, 0, -1),
    );

    drupal_add_js(array('langtests' => $settings), 'setting');
    drupal_add_js(drupal_get_path('module', 'langtests') . '/includes/testtype.mtr.js', 'file');

  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $input = array();
  $result = 0;
  $ok = 0;

  $keys = $form_state['values']['keys'];
  $input['keys'] = $keys;

  for ($i = 1; $i <= $form_state['values']['elements_number']; $i++) {
    $name = 'f' . $i;
    $correct = FALSE;

    $fi = $form_state['values'][$name];
    $input[$name] = $fi;

    // Check answers.
    if (is_numeric($fi) && 'r' . $fi == $keys[($i - 1)]) {
      $correct = TRUE;
      $ok++;
    }

    // Save information about correct and incorrect answers.
    $form_state[$name]['correct'] = $correct;
  }

  if ($ok == $form_state['values']['elements_number']) {
    $message = t('Congratulations! All your answers are correct!');
    $type = 'status';
    $result = 1;
  }
  else {
    $message = t('You scored !ok out of !total.',
                  array(
                    '!ok' => $ok,
                    '!total' => $form_state['values']['elements_number'],
                  )
                );
    $type = 'error';
  }

  $form_state['change_button'] = TRUE;

  $testresult['result'] = $result;
  $testresult['input'] = $input;

  drupal_set_message(check_plain($message), $type);
}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  $left_part = array();
  $right_part = array();
  $n = count($elements) / 2;
  for ($i = 1; $i <= $n; $i++) {
    $left_part[$i] = $elements['l' . $i];
    $right_part[$i] = $elements[$input['keys'][($i - 1)]];
  }

  $maup = '<div style="position: relative;"><table class="matcher">';
  $form['table_start'] = array('#markup' => $maup);

  $correct_index = '';

  foreach ($left_part as $i => $val) {
    $name = 'f' . $i;
    $prefix = '<tr><td>' . $i . '</td><td class="matcher-source">' . $val . '</td><td class="rathtd" width="30%">&nbsp;</td><td class="matcher-hidden">';
    $suffix = '</td><td class="matcher-target">' . $right_part[$i] . '</td></tr>';

    $style = (('r' . $input[$name]) == $input['keys'][$i - 1]) ? 'correct' : 'mistake';

    if ($style == 'correct') {
      $correct_index .= $input[$name] . ',';
    }

    $form[$name] = array(
      '#type' => 'textfield',
      '#size' => 1,
      '#maxlength' => 3,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#default_value' => isset($input[$name]) ? $input[$name] : NULL,
      '#attributes' => array('class' => array('matcher-form-text', $style)),
    );
  }

  $form['table_stop'] = array(
    // Close table.
    '#markup' => '</table><div id="mtrholder" style="position: absolute;"></div>',
  );

  // Add Raphael Library.
  if ($raphpath = libraries_get_path('raphael')) {
    drupal_add_js($raphpath . '/raphael-min.js');
  }

  // Add javascript stuff.
  $right_keys = '';
  foreach ($input['keys'] as $k) {
    // Remove r from key name.
    $right_keys .= drupal_substr($k, 1) . ',';
  }

  $settings = array(
    'submitted' => 1,
    'correct_index' => drupal_substr($correct_index, 0, -1),
    'right_keys' => drupal_substr($right_keys, 0, -1),
  );

  drupal_add_js(array('langtests' => $settings), 'setting');
  drupal_add_js(drupal_get_path('module', 'langtests') . '/includes/testtype.mtr.js', 'file');

}
