<?php
/**
 * @file
 * Functions for tests with type "Radios and checkboxes"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  $elements = $options['elements'];
  $sid = $options['sid'];

  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = $form_state['step'];
  if (is_array($elements) && isset($elements[1]['title'])) {
    $step = 2;
  }

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
  );

  if ($step == 1) {
    $form['fieldset']['count'] = array(
      '#type' => 'select',
      '#title' => t('Number of questions'),
      '#default_value' => 3,
      '#multiple' => FALSE,
      '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5),
      '#required' => FALSE,
      '#description' => t('Select the desired number of questions in this test item.'),
    );
  }
  elseif ($step == 2) {
    $count = 1;
    if (is_array($elements)) {
      $count = count($elements);
    }
    if (isset($form_state['values']['fieldset']['count'])) {
      $count = $form_state['values']['fieldset']['count'];
    }

    for ($i = 1; $i <= $count; $i++) {
      $title = '';
      $task_opts = '';

      // Set the title.
      if (is_array($elements) && isset($elements[$i]['title'])) {
        $title = $elements[$i]['title'];
      }

      // Set task options as a string.
      if (isset($elements[$i]['options']) && is_array($elements[$i]['options'])) {
        foreach ($elements[$i]['options'] as $v) {
          $correct = '';
          if ($v[1]) {
            $correct = '# ';
          }

          if (empty($task_opts)) {
            $task_opts = $correct . $v[0];
          }
          else {
            $task_opts .= "\n" . $correct . $v[0];
          }
        }
      }

      $form['fieldset'][$i]['title'] = array(
        '#type' => 'textfield',
        '#title' => t('Question') . ' ' . $i,
        '#default_value' => $title,
        '#size' => 64,
        '#maxlength' => 256,
        '#required' => FALSE,
        '#description' => t('Enter the question.'),
      );
      $form['fieldset'][$i]['options'] = array(
        '#type' => 'textarea',
        '#title' => t('Answer options'),
        '#default_value' => $task_opts,
        '#description' => t('Enter each option on a separate line. Mark CORRECT answer with leading # symbol.'),
      );
    }
    // Fields for entering extra question.
    $form['fieldset']['markup2'] = array(
      '#markup' => '<hr><br />' . t('You can add one more question here. Leave the fields blank if not required.') . '<br />',
    );
    $form['fieldset'][$count + 1]['title'] = array(
      '#type' => 'textfield',
      '#title' => t('Question') . ' ' . $i,
      '#default_value' => '',
      '#size' => 64,
      '#maxlength' => 256,
      '#required' => FALSE,
      '#description' => t('Enter the question.'),
    );
    $form['fieldset'][$count + 1]['options'] = array(
      '#type' => 'textarea',
      '#title' => t('Answer options'),
      '#default_value' => '',
      '#description' => t('Enter each option on a separate line. Mark CORRECT answer with leading # symbol.'),
    );
  }

  if ($step == 1) {
    // Next step button.
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#submit' => array('langtests_rnc_next'),
    );
  }
  elseif ($step == 2) {
    // Submit button.
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
  }
  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];

  // Make elements array.
  $elements = array();
  $message = array();

  $i = 1;
  foreach ($form_state['values']['fieldset'] as $k => $v) {
    if (is_numeric($k) && !empty($v['title'])) {
      $title = trim($v['title']);
      if ($title) {
        $elements[$i]['title'] = $title;
      }
      else {
        // If there is empty title, skip this question.
        continue;
      }

      $opts = array();
      if (isset($v['options'])) {
        $opts = explode("\n", str_replace(array("\r\n", "\n\r"), "\n", $v['options']));
      }

      $j = 0; $c = 0;
      foreach ($opts as $v1) {
        $v1 = trim($v1);
        $correct = FALSE;
        if (!empty($v1) && $v1{0} == '#') {
          // Correct answer is marked with # symbol.
          $correct = TRUE;
          $v1 = trim(drupal_substr($v1, 1));
          $c++;
        }
        if ($v1) {
          $elements[$i]['options'][$j][0] = $v1;
          $elements[$i]['options'][$j][1] = $correct;
        }
        $j++;
      }
      if ($c < 1) {
        $message[$i] = t('You have to mark at least ONE correct answer in the Question') . ' ' . $i;
      }
      $i++;
    }
  }

  $options['description'] = $description;
  $options['elements'] = $elements;
  $options['message'] = $message;

}

/**
 * Submit handler for the "Next step" button.
 */
function langtests_rnc_next(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step = 2;
  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $format = (isset($elements['format'])) ? $elements['format'] : filter_fallback_format();
  $next_tid = $options['next_tid'];

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  if (!empty($elements)) {
    $correct_answers = array();

    foreach ($elements as $i => $val) {
      $name = 'radio_check_' . $i;

      // If only one correct answer make radio buttons, else -- checkboxes.
      $count = 0;
      foreach ($val['options'] as $option) {
        if ($option[1]) {
          $count++;
        }
      }
      $element_type = '';
      switch ($count) {
        case 0:
          drupal_set_message(check_plain(t('No correct answer defined in question') . ' ' . $i), 'error');
          break;

        case 1:
          $element_type = 'radios';
          break;

        default:
          $element_type = 'checkboxes';
      }

      $style = isset($form_state['stored_answers'][$i]) ? $form_state['stored_answers'][$i] : NULL;

      $suffix = '';
      if (!is_null($style)) {
        $style = ($style) ? '<font class = "correct">' : '<font class = "mistake">';
        $suffix = '</font>';
      }

      $form[$name] = array(
        '#type' => $element_type,
        '#title' => $elements[$i]['title'],
        '#prefix' => $style,
        '#suffix' => $suffix,
      );

      $j = 1; $k = 1;
      foreach ($val['options'] as $option) {
        // Add options.
        $form[$name]['#options'][$k] = $option[0];
        $k++;
        // Make array of correct answers.
        if ($option[1]) {
          $correct_answers[$i][] = $j;
        }
        $j++;
      }
    }

    // Send parameters to _submit function.
    $form['elements_number'] = array('#type' => 'value', '#value' => $i);
    $form['answers'] = array('#type' => 'value', '#value' => $correct_answers);

    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['change_button'])) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
          '#prefix' => '<br /><br />',
        );
      }
      else {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#prefix' => '<br /><br />',
          '#submit' => $submit,
        );
      }
    }
  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $input = array();
  $result = 0;
  $ok = 0;

  for ($i = 1; $i <= $form_state['values']['elements_number']; $i++) {
    $name = 'radio_check_' . $i;
    $correct = FALSE;

    $fi = isset($form_state['values'][$name]) ? $form_state['values'][$name] : NULL;
    $input[$i] = $fi;

    if (is_array($fi)) {
      // Check answers for checkboxes.
      $fi_checked = array();
      foreach ($fi as $k => $v) {
        if ($v) {
          $fi_checked[] = $v;
        }
      }
      if ($fi_checked == $form_state['values']['answers'][$i]) {
        $correct = TRUE;
        $ok++;
      }
    }
    else {
      // Check answers for radios.
      foreach ($form_state['values']['answers'][$i] as $answer) {
        if (!is_null($fi) && $fi != '' && $fi == $answer) {
          $correct = TRUE;
          $ok++;
          break;
        }
      }
    }

    // Save information about correct and incorrect answers.
    $form_state['stored_answers'][$i] = $correct;
  }

  if ($ok == $form_state['values']['elements_number']) {
    $message = t('Congratulations! All your answers are correct!');
    $type = 'status';
    $result = 1;
  }
  else {
    $message = t('You scored !ok out of !total.',
                  array(
                    '!ok' => $ok,
                    '!total' => $form_state['values']['elements_number'],
                  )
                );
    $type = 'error';
  }

  $form_state['change_button'] = TRUE;

  $testresult['result'] = $result;
  $testresult['input'] = $input;

  drupal_set_message(check_plain($message), $type);
}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $format = (isset($elements['format'])) ? $elements['format'] : filter_fallback_format();
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  foreach ($elements as $i => $val) {
    $name = 'radio_check_' . $i;

    $correct_answers = array();
    $correct_answer = NULL;

    // If only one correct answer make radio buttons, else -- checkboxes.
    $count = 0; $j = 1;
    foreach ($val['options'] as $option) {
      if ($option[1]) {
        $count++;
        $correct_answers[$j] = $j;
        $correct_answer = $j;
      }
      else {
        $correct_answers[$j] = 0;
      }
      $j++;
    }

    $element_type = '';
    switch ($count) {
      case 0:
        drupal_set_message(check_plain(t('No correct answer defined in question') . ' ' . $i), 'error');
        break;

      case 1:
        $element_type = 'radios';
        break;

      default:
        $element_type = 'checkboxes';
    }

    $style = NULL;
    if ($count > 1 && isset($input[$i])) {
      // Check answers for checkboxes.
      $style = ($input[$i] == $correct_answers) ? TRUE : FALSE;
    }
    elseif ($count == 1 && isset($input[$i])) {
      // Check answers for radios.
      $style = ($input[$i] == $correct_answer) ? TRUE : FALSE;
    }

    $suffix = '';
    if (!is_null($style)) {
      $style = ($style) ? '<font class = "correct">' : '<font class = "mistake">';
      $suffix = '</font>';
    }

    $form[$name] = array(
      '#type' => $element_type,
      '#title' => $elements[$i]['title'],
      '#prefix' => $style,
      '#suffix' => $suffix,
      '#default_value' => isset($input[$i]) ? $input[$i] : NULL,
    );

    $k = 1;
    foreach ($val['options'] as $option) {
      // Add options.
      $form[$name]['#options'][$k] = $option[0];
      $k++;
    }
  }
}
