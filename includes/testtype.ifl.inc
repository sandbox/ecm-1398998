<?php
/**
 * @file
 * Functions for tests with type "Input fields"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  $elements = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $sid = $options['sid'];
  $placeholder = '###';

  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = &$form_state['step'];

  $task = '';
  $format = NULL;

  // Set task and format as strings.
  if (!empty($form_state['stored_task'])) {
    $task = $form_state['stored_task']['value'];
    $format = $form_state['stored_task']['format'];
  }
  elseif (is_array($elements)) {
    foreach ($elements as $k => $v) {
      if (is_numeric($k)) {
        // This is to avoid unnecessary inserting of placeholder when
        // there are no fields in the task.
        if ($k == 0 && !isset($v['prefix']) && isset($v['suffix'])) {
          $task = $v['suffix'];
        }
        else {
          $prefix = (isset($v['prefix'])) ? $v['prefix'] : '';
          $suffix = (isset($v['suffix'])) ? $v['suffix'] : '';
          $task .= $prefix . $placeholder . $suffix;
        }
      }
    }
    if (isset($elements['format'])) {
      $format = $elements['format'];
    }
  }

  // Set correct answers as strings.
  $count = substr_count($task, $placeholder);
  $fields_opts = array();
  for ($i = 1; $i <= $count; $i++) {
    if (isset($form_state['correct_answers'][$i])) {
      $fields_opts[$i] = $form_state['correct_answers'][$i];
    }
    elseif (is_array($correct_answers) && isset($correct_answers[$i])) {
      // Show options stored in database.
      $fields_opts[$i] = implode("\n", $correct_answers[$i]);
    }
  }

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  if ($step == 1) {
    $title = t('Enter the task');
  }
  if ($step == 2) {
    $title = t('Enter the correct answers');
  }

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => $title,
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
  );

  if ($step == 1) {
    $form['fieldset']['task'] = array(
      '#type' => 'text_format',
      '#title' => t('Use ### as a placeholder for input fields.'),
      '#default_value' => $task,
      '#required' => FALSE,
      '#description' => t('At the next step you can enter answer options.'),
      '#format' => $format,
      '#suffix' => '<br /><br />',
    );
    $form['fieldset']['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next step'),
      '#submit' => array('langtests_ifl_next'),
    );
    $form_state['correct_answers'] = $fields_opts;
  }
  elseif ($step == 2) {
    $show_task = check_markup($form_state['stored_task']['value'], $form_state['stored_task']['format']);
    $count = substr_count($show_task, $placeholder);

    if ($count > 0) {
      // Replace placeholder with the marker [FIELD N]
      for ($i = 1; $i <= $count; $i++) {
        $pos = strpos($show_task, $placeholder);
        $show_task = drupal_substr($show_task, 0, $pos) . '<strong>[' . t('FIELD') . ' ' . $i . ']</strong>' . drupal_substr($show_task, $pos + 3);
      }
    }

    $form['fieldset']['show_task'] = array(
      '#markup' => $show_task,
    );

    if ($count > 0) {
      for ($i = 1; $i <= $count; $i++) {
        $form['fieldset']['options'][$i] = array(
          '#type' => 'textarea',
          '#title' => t('Field') . ' ' . $i,
          '#default_value' => isset($fields_opts[$i]) ? $fields_opts[$i] : '',
          '#description' => t('It is possible to have several correct answers for each input field. Enter each correct answer in a separate line.'),
        );
      }
    }
    else {
      $form['fieldset']['no_fields'] = array(
        '#markup' => t('There are no fields in your task. Use ### as a placeholder for input fields.') . '<br />',
      );
    }

    $form['fieldset']['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Previous step'),
      '#submit' => array('langtests_ifl_prev'),
      '#prefix' => '<br />',
    );
  }

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];
  $step = $form_state['step'];

  $fields_opts = array();

  if ($step == 1) {
    $raw_task = $form_state['values']['fieldset']['task']['value'];
    $format = $form_state['values']['fieldset']['task']['format'];
    if (isset($form_state['correct_answers'])) {
      $fields_opts = $form_state['correct_answers'];
    }
  }
  elseif ($step == 2) {
    if (isset($form_state['stored_task'])) {
      $raw_task = $form_state['stored_task']['value'];
      $format = $form_state['stored_task']['format'];
    }
    $fields_opts = $form_state['values']['fieldset']['options'];
  }

  $placeholder = '###';
  $count = substr_count($raw_task, $placeholder);

  $elements = array();

  // Make elements array.
  for ($i = 1; $i <= $count; $i++) {
    $pos = strpos($raw_task, $placeholder);
    $prefix = drupal_substr($raw_task, 0, $pos);
    $raw_task = drupal_substr($raw_task, $pos + 3);
    $elements[$i]['prefix'] = $prefix;
  }
  $elements[$count]['suffix'] = $raw_task;
  $elements['format'] = $format;

  // Make correct_answers array.
  $message = array();
  $correct_answers = array();

  foreach ($fields_opts as $k => $v) {
    // Discard last options when the number of fields has been reduced.
    if ($k > $count) {
      break;
    }

    $opts = explode("\n", str_replace(array("\r\n", "\n\r"), "\n", $v));

    $i = 0; $c = 0;
    foreach ($opts as $v1) {
      $v1 = trim($v1);
      if ($v1) {
        // Remove whitespaces.
        $v1 = preg_replace('~\s+~', ' ', $v1);

        $correct_answers[$k][$i] = $v1;
        $c++;
      }
      $i++;
    }
    if ($c == 0) {
      $message[$k] = t('You have to provide at least ONE correct answer for the Field') . ' ' . $k;
    }
  }

  $options['description'] = $description;
  $options['elements'] = $elements;
  $options['correct_answers'] = $correct_answers;
  $options['message'] = $message;

}

/**
 * Submit handler for the "Next step" button.
 */
function langtests_ifl_next(&$form, &$form_state) {
  $form_state['stored_task'] = $form_state['values']['fieldset']['task'];
  $form_state['step'] = 2;
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Previous step" button.
 */
function langtests_ifl_prev($form, &$form_state) {
  if (isset($form_state['values']['fieldset']['options'])) {
    foreach ($form_state['values']['fieldset']['options'] as $k => $v) {
      $form_state['correct_answers'][$k] = $v;
    }
  }
  $form_state['step'] = 1;
  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $format = (isset($elements['format'])) ? $elements['format'] : filter_fallback_format();
  $next_tid = $options['next_tid'];

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  if (!isset($elements[0])) {
    $i = 0; $n = 0;
    foreach ($elements as $i => $val) {
      if (is_numeric($i)) {
        // The number of textfields.
        $n++;
        $name = 'textfield_' . $i;

        $style = isset($form_state['stored_answers'][$i]) ? $form_state['stored_answers'][$i] : NULL;
        if (!is_null($style)) {
          $style = ($style) ? 'correct' : 'mistake';
        }

        $prefix = '';
        $newline = FALSE;
        if (isset($val['prefix'])) {
          // If the string starts with newline symbol (\r or \n)...
          if (strspn($val['prefix'], "\r\n")) {
            $newline = TRUE;
          }

          $prefix = check_markup($val['prefix'], $format);
          // Remove unnecessary <p> tags from the beginning...
          if (strpos($prefix, '<p>') === 0) {
            $prefix = drupal_substr($prefix, 3);
          }
          // ...and the end of the string...
          if (strrpos($prefix, '</p>') == (drupal_strlen($prefix) - 5)) {
            $prefix = drupal_substr($prefix, 0, -5);
          }

          // ...add a <br /> tag at the beginning.
          if ($newline) {
            $prefix = '<br />' . $prefix;
          }
        }
        $suffix = '';
        $newline = FALSE;
        if (isset($val['suffix'])) {
          // If the string starts with newline symbol (\r or \n)...
          if (strspn($val['suffix'], "\r\n")) {
            $newline = TRUE;
          }

          $suffix = check_markup($val['suffix'], $format);
          // Remove unnecessary <p> tags from the beginning...
          if (strpos($suffix, '<p>') === 0) {
            $suffix = drupal_substr($suffix, 3);
          }
          // ...and the end of the string.
          if (strrpos($suffix, '</p>') == (drupal_strlen($suffix) - 5)) {
            $suffix = drupal_substr($suffix, 0, -5);
          }

          // ...add a <br /> tag at the beginning.
          if ($newline) {
            $suffix = '<br />' . $suffix;
          }
        }

        $form[$name] = array(
          '#type' => 'textfield',
          '#field_prefix' => $prefix,
          '#field_suffix' => $suffix,
          '#attributes' => !is_null($style) ? array('class' => array($style)) : '',
          '#size' => 25,
          // Non-standard property, but needed for theming.
          '#inline' => TRUE,
        );
      }
    }

    // Send parameters to _submit function.
    $form['elements_number'] = array('#type' => 'value', '#value' => $n);
    $form['answers'] = array('#type' => 'value', '#value' => $correct_answers);

    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['change_button'])) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
          '#prefix' => '<br /><br />',
        );
      }
      else {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#prefix' => '<br /><br />',
          '#submit' => $submit,
        );
      }
    }
  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $input = array();
  $result = 0;
  $ok = 0;

  for ($i = 1; $i <= $form_state['values']['elements_number']; $i++) {
    $name = 'textfield_' . $i;
    $correct = FALSE;

    $fi = isset($form_state['values'][$name]) ? trim($form_state['values'][$name]) : NULL;
    // Remove whitespaces.
    if (!is_null($fi)) {
      $fi = preg_replace('~\s+~', ' ', $fi);
    }
    $input[$i] = $fi;

    // Check answers.
    foreach ($form_state['values']['answers'][$i] as $answer) {
      // Case-insensitive comparison.
      if (!is_null($fi) && $fi != '' && strcasecmp($fi, $answer) == 0) {
        $correct = TRUE;
        $ok++;
        break;
      }
    }

    // Save information about correct and incorrect answers.
    $form_state['stored_answers'][$i] = $correct;
  }

  if ($ok == $form_state['values']['elements_number']) {
    $message = t('Congratulations! All your answers are correct!');
    $type = 'status';
    $result = 1;
  }
  else {
    $message = t('You scored !ok out of !total.',
                  array(
                    '!ok' => $ok,
                    '!total' => $form_state['values']['elements_number'],
                  )
                );
    $type = 'error';
  }

  $form_state['change_button'] = TRUE;

  $testresult['result'] = $result;
  $testresult['input'] = $input;

  drupal_set_message(check_plain($message), $type);
}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $elements = $options['elements'];
  $correct_answers = $options['correct_answers'];
  $format = isset($elements['format']) ? $elements['format'] : filter_fallback_format();
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#title' => $description,
  );

  $i = 0; $n = 0;
  foreach ($elements as $i => $val) {
    if (is_numeric($i)) {
      // The number of textfields.
      $n++;
      $name = 'textfield_' . $i;

      $style = NULL;
      if (isset($input[$i]) && isset($correct_answers[$i])) {
        $style = in_array($input[$i], $correct_answers[$i]) ? 'correct' : 'mistake';
      }

      $prefix = '';
      $newline = FALSE;
      if (isset($val['prefix'])) {
        // If the string starts with newline symbol (\r or \n)...
        if (strspn($val['prefix'], "\r\n")) {
          $newline = TRUE;
        }

        $prefix = check_markup($val['prefix'], $format);
        // Remove unnecessary <p> tags from the beginning...
        if (strpos($prefix, '<p>') === 0) {
          $prefix = drupal_substr($prefix, 3);
        }
        // ...and the end of the string...
        if (strrpos($prefix, '</p>') == (drupal_strlen($prefix) - 5)) {
          $prefix = drupal_substr($prefix, 0, -5);
        }

        // ...add a <br /> tag at the beginning.
        if ($newline) {
          $prefix = '<br />' . $prefix;
        }
      }
      $suffix = '';
      $newline = FALSE;
      if (isset($val['suffix'])) {
        // If the string starts with newline symbol (\r or \n)...
        if (strspn($val['suffix'], "\r\n")) {
          $newline = TRUE;
        }

        $suffix = check_markup($val['suffix'], $format);
        // Remove unnecessary <p> tags from the beginning...
        if (strpos($suffix, '<p>') === 0) {
          $suffix = drupal_substr($suffix, 3);
        }
        // ...and the end of the string.
        if (strrpos($suffix, '</p>') == (drupal_strlen($suffix) - 5)) {
          $suffix = drupal_substr($suffix, 0, -5);
        }

        // ...add a <br /> tag at the beginning.
        if ($newline) {
          $suffix = '<br />' . $suffix;
        }
      }

      $form[$name] = array(
        '#type' => 'textfield',
        '#field_prefix' => $prefix,
        '#field_suffix' => $suffix,
        '#default_value' => isset($input[$i]) ? $input[$i] : NULL,
        '#attributes' => !is_null($style) ? array('class' => array($style)) : '',
        '#size' => 25,
        // Non-standard property, but needed for theming.
        '#inline' => TRUE,
      );
    }
  }
}
