<?php
/**
 * @file
 * Functions for tests with type "Select explanation"
 */

/**
 * Form to edit test item.
 */
function langtests_testtype_editor(&$form, &$form_state, $options = array()) {
  $type = $options['type'];
  $description = $options['description'];
  // An approximate number of answer options.
  $n = isset($options['attributes']['n']) ? $options['attributes']['n'] : 5;
  // The number of steps in one test set.
  $inset = isset($options['attributes']['inset']) ? $options['attributes']['inset'] : 5;
  // Link to dictionary.
  $dictlink = isset($options['attributes']['dictionary']) ? $options['attributes']['dictionary'] : 'my';
  if ($dictlink == 'my') {
    // Use own dictionary for this test.
    $dictionary = $options['dictionary'];
  }
  $sid = $options['sid'];

  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = $form_state['step'];

  if (isset($form_state['stored_values']['dictionary'])) {
    $dictionary = $form_state['stored_values']['dictionary'];
  }
  else {
    $form_state['stored_values']['dictionary'] = $dictionary;
  }

  if (isset($form_state['stored_values']['n'])) {
    $n = $form_state['stored_values']['n'];
  }
  else {
    $form_state['stored_values']['n'] = $n;
  }

  if (isset($form_state['stored_values']['inset'])) {
    $n = $form_state['stored_values']['inset'];
  }
  else {
    $form_state['stored_values']['inset'] = $inset;
  }

  $count = is_array($dictionary) ? count($dictionary) : 0;
  $form_state['stored_values']['count'] = $count;

  $form['markup1'] = array(
    '#markup' => '<h1>' . check_plain($type) . '</h1>',
  );

  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => $description,
    '#size' => 64,
    '#maxlength' => 256,
    '#required' => FALSE,
    '#description' => t('Enter the short explanation or task topic.'),
  );

  $form['n'] = array(
    '#type' => 'select',
    '#title' => t('Number of answer options'),
    '#default_value' => $n,
    '#multiple' => FALSE,
    '#options' => array(
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      8 => 8,
      9 => 9,
      10 => 10,
    ),
    '#required' => FALSE,
    '#description' => t('Select an approximate number of answer options<br />(one option more or less can be shown on page).'),
  );

  $form['inset'] = array(
    '#type' => 'select',
    '#title' => t('Number of steps'),
    '#default_value' => $inset,
    '#multiple' => FALSE,
    '#options' => array(
      1 => 1,
      2 => 2,
      3 => 3,
      4 => 4,
      5 => 5,
      6 => 6,
      7 => 7,
      8 => 8,
      9 => 9,
      10 => 10,
    ),
    '#required' => FALSE,
    '#description' => t('Select the number of steps in one test set.'),
  );

  // Set #tree to be able to access elements in the fieldset.
  $form['#tree'] = TRUE;

  $form['fieldset'] = array(
    '#type' => 'fieldset',
    '#prefix' => '<div id="task-wrapper">',
    '#suffix' => '</div>',
    '#title' => t('Enter words and explanations'),
  );

  if ($count) {
    $form['fieldset']['markup_t11'] = array(
      '#markup' => '<table>',
    );
    for ($i = 1; $i <= $count; $i++) {
      $form['fieldset']['word' . $i] = array(
        '#type' => 'textfield',
        '#title' => ($i == 1) ? t('Word') : '',
        '#default_value' => isset($dictionary[$i][0]) ? $dictionary[$i][0] : '',
        '#size' => 32,
        '#maxlength' => 64,
        '#prefix' => '<tr><td>',
        '#suffix' => '</td>',
      );
      $form['fieldset']['exp' . $i] = array(
        '#type' => 'textfield',
        '#title' => ($i == 1) ? t('Explanation') : '',
        '#default_value' => isset($dictionary[$i][1]) ? $dictionary[$i][1] : '',
        '#maxlength' => 256,
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>',
      );
    }
    $form['fieldset']['markup_t12'] = array(
      '#markup' => '</table>',
    );
  }

  // Fields for entering extra word.
  if ($count) {
    $form['fieldset']['markup2'] = array(
      '#markup' => '<br /><br />' . t('You can add a word and explanation here. Leave the fields blank if not required.') . '<br />',
    );
  }

  $form['fieldset']['markup_t1'] = array(
    '#markup' => '<table><tr>',
  );
  $form['fieldset']['word' . ($count + 1)] = array(
    '#type' => 'textfield',
    '#title' => t('Word'),
    '#size' => 32,
    '#maxlength' => 64,
    '#description' => t('Enter the word, set expression or conception.'),
    '#prefix' => '<td>',
    '#suffix' => '</td>',
  );
  $form['fieldset']['exp' . ($count + 1)] = array(
    '#type' => 'textfield',
    '#title' => t('Explanation'),
    '#maxlength' => 256,
    '#description' => t('Enter the explanation.'),
    '#prefix' => '<td>',
    '#suffix' => '</td>',
  );
  $form['fieldset']['markup_t2'] = array(
    '#markup' => '</tr></table>',
  );

  // Add a word button.
  $form['fieldset']['next'] = array(
    '#type' => 'submit',
    '#value' => t('Add a word'),
    '#submit' => array('langtests_sxp_next'),
  );
  // Delete a word button.
  if ($count) {
    $form['fieldset']['prev'] = array(
      '#type' => 'submit',
      '#value' => t('Delete last word'),
      '#submit' => array('langtests_sxp_prev'),
      '#limit_validation_errors' => array(),
    );
  }

  // Submit button.
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // Cancel button.
  $form['cancel'] = array(
    '#markup' => '&nbsp;&nbsp;' . l(t('Cancel'), 'admin/langtests/' . $sid, array(
      'attributes' => array('class' => array('cancel-button')))),
  );
}

/**
 * Validate handler.
 */
function langtests_testtype_validator($form, &$form_state) {
  $count = $form_state['stored_values']['count'];

  for ($i = 1; $i <= $count; $i++) {
    if (trim($form_state['values']['fieldset']['word' . $i]) == '') {
      form_set_error('fieldset][word' . $i, t('You must fill in the field "Word') . ' ' . $i . '"');
    }
    if (trim($form_state['values']['fieldset']['exp' . $i]) == '') {
      form_set_error('fieldset][exp' . $i, t('You must fill in the field "Explanation') . ' ' . $i . '"');
    }
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_submitter($form, &$form_state, &$options) {
  $description = $form_state['values']['description'];
  $n = $form_state['values']['n'];
  $inset = $form_state['values']['inset'];
  $count = $form_state['stored_values']['count'];

  $ret = 0;
  if ($count == 0 && trim($form_state['values']['fieldset']['word1']) == '') {
    form_set_error('fieldset][word1', t('You must fill in the field "Word"'));
    $ret++;
  }
  if ($count == 0 && trim($form_state['values']['fieldset']['exp1']) == '') {
    form_set_error('fieldset][exp1', t('You must fill in the field "Explanation"'));
    $ret++;
  }
  if ($ret) {
    return;
  }

  // Make dictionary array.
  $dictionary = array();
  for ($i = 1; $i <= $count; $i++) {
    $wordi = isset($form_state['values']['fieldset']['word' . $i]) ? trim($form_state['values']['fieldset']['word' . $i]) : NULL;
    if ($wordi) {
      // Remove whitespaces.
      $wordi = preg_replace('~\s+~', ' ', $wordi);
    }
    $expi = isset($form_state['values']['fieldset']['exp' . $i]) ? trim($form_state['values']['fieldset']['exp' . $i]) : NULL;
    // Remove whitespaces.
    if (!is_null($expi)) {
      $expi = preg_replace('~\s+~', ' ', $expi);
    }

    $dictionary[$i] = array($wordi, $expi);
  }

  $wordn = isset($form_state['values']['fieldset']['word' . ($count + 1)]) ? trim($form_state['values']['fieldset']['word' . ($count + 1)]) : NULL;
  $expn = isset($form_state['values']['fieldset']['exp' . ($count + 1)]) ? trim($form_state['values']['fieldset']['exp' . ($count + 1)]) : NULL;
  if ($wordn && $expn) {
    $dictionary[$count + 1] = array($wordn, $expn);
  }

  // Make attributes array.
  $attributes = array();
  $attributes['n'] = $n;
  $attributes['inset'] = $inset;
  $attributes['dictionary'] = 'my';

  $options['description'] = $description;
  $options['dictionary'] = $dictionary;
  $options['attributes'] = $attributes;

}

/**
 * Submit handler for the "Add a word" button.
 */
function langtests_sxp_next(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step++;

  $count = $form_state['stored_values']['count'] + 1;

  if (trim($form_state['values']['fieldset']['word' . $count]) && trim($form_state['values']['fieldset']['exp' . $count])) {
    $form_state['stored_values']['dictionary'][$count][0] = $form_state['values']['fieldset']['word' . $count];
    $form_state['stored_values']['dictionary'][$count][1] = $form_state['values']['fieldset']['exp' . $count];
  }
  else {
    drupal_set_message(t('You must fill in both fields: "Word" and "Explanation"'), 'error');
  }

  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the "Delete last word" button.
 */
function langtests_sxp_prev(&$form, &$form_state) {
  $step = &$form_state['step'];
  $step--;

  $count = $form_state['stored_values']['count'];

  unset($form_state['stored_values']['dictionary'][$count]);
  // Also clear new word fields.
  unset($form_state['input']['fieldset']['word' . $count]);
  unset($form_state['input']['fieldset']['exp' . $count]);

  $form_state['rebuild'] = TRUE;
}

/**
 * Displays test item.
 */
function langtests_testtype_display(&$form, &$form_state, $options = array()) {
  $description = $options['description'];
  $dictionary = $options['dictionary'];
  // An approximate number of answer options.
  $nn = isset($options['attributes']['n']) ? $options['attributes']['n'] : 5;
  $n = count($dictionary) > $nn ? $nn : count($dictionary);
  // The number of words in one test set.
  $inset = isset($options['attributes']['inset']) ? $options['attributes']['inset'] : 5;
  $next_tid = $options['next_tid'];
  $tid = $options['tid'];

  if (!empty($dictionary)) {
    if (empty($form_state['step'])) {
      $form_state['step'] = 1;
    }
    $step = $form_state['step'];

    if (!isset($form_state['stored_values']['answers'])) {
      $form['ldescription'] = array(
        '#type' => 'item',
        '#title' => $description,
      );
      $form['count'] = array(
        '#markup' => '<div class="word-count">' . $step . ' / ' . $inset . '</div><br />',
      );
    }
    else {
      $form['description'] = array(
        '#type' => 'item',
        '#title' => t('Your correct answers:'),
      );
    }

    $keys = array();
    if (isset($_SESSION['words' . $tid])) {
      $keys = $_SESSION['words' . $tid];
    }
    else {
      // Keys for words.
      $keys[0] = array_rand($dictionary, $inset);
      for ($i = 1; $i <= $inset; $i++) {
        $arr = array_rand($dictionary, $n);
        // Add correct answer.
        if (!in_array($keys[0][$i - 1], $arr)) {
          $arr[] = $keys[0][$i - 1];
        }
        shuffle($arr);
        // Keys for answer options.
        $keys[$i] = $arr;
      }
      $_SESSION['words' . $tid] = $keys;
    }

    $form['table_start'] = array(
      '#markup' => '<table class="word-translation">',
    );

    if (!isset($form_state['stored_values']['answers'])) {
      $form['word'] = array(
        '#markup' => '<tr><td class="word-tr-word">' . $dictionary[$keys[0][$step - 1]][0] . '</td>',
      );
      $form['expl'] = array(
        '#markup' => '<td class="word-tr-expl">',
      );

      $default_value = isset($form_state['step_information'][$step]['stored_values']['radios']) ? $form_state['step_information'][$step]['stored_values']['radios'] : -1;
      // This is instead of the #default_value parameter.
      $form_state['input']['radios'] = $default_value;

      $form['radios'] = array(
        '#type' => 'radios',
        '#title' => '',
      );

      foreach ($keys[$step] as $v) {
        $form['radios']['#options'][$v] = $dictionary[$v][1];
      }
      $form['radios']['#options']['dn'] = t("I don't know");

      $form['td_stop'] = array(
        '#markup' => '</td></tr>',
      );
    }
    else {
      // Check button pressed.
      $ok = 0;
      foreach ($form_state['stored_values']['answers'] as $k => $v) {
        if ($v == $keys[0][$k]) {
          $form['word' . $k] = array(
            '#markup' => '<tr><td class="word-tr-word word-tr-word-correct">' . $dictionary[$keys[0][$k]][0] . '</td>',
          );
          $form['expl' . $k] = array(
            '#markup' => '<td class="word-tr-expl word-tr-expl-correct">' . $dictionary[$keys[0][$k]][1] . '</td></tr>',
          );
          $ok++;
        }
      }
      if ($ok == 0) {
        $form['description']['#title'] = t('Unfortunately, there are no correct answers.');
      }
      if (isset($_SESSION['words' . $tid])) {
        unset($_SESSION['words' . $tid]);
      }
    }

    $form['table_stop'] = array(
      // Close table.
      '#markup' => '</table><br />',
    );

    // Send parameters to _submit function.
    $form['inset'] = array('#type' => 'value', '#value' => $inset);

    // Previous button.
    if ($step > 1 && !isset($form_state['stored_values']['answers'])) {
      $form['prev'] = array(
        '#type' => 'submit',
        '#value' => t('<< Previous'),
        '#name' => 'prev',
        '#submit' => array('langtests_sxp_display_prev'),
        '#limit_validation_errors' => array(),
      );
    }

    // Next button.
    if ($step < $inset && !isset($form_state['stored_values']['answers'])) {
      $form['next'] = array(
        '#type' => 'submit',
        '#value' => t('Next >>'),
        '#name' => 'next',
        '#submit' => array('langtests_sxp_display_next'),
      );
    }

    // Submit or Go-to-next-test or Go-back button.
    if ($options['uid'] == 0) {
      $form['register'] = array(
        '#markup' => '<br /><br />' . t('Please, !login or !reg to make it possible to pass the tests.', array(
          '!login' => l(t('log in'), 'user'),
          '!reg' => l(t('register'), 'user/register'))
        ),
      );
    }
    else {
      if (!isset($form_state['stored_values']['answers']) && $step == $inset) {
        // Submit button.
        $form['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Check'),
        );
      }
      if (isset($form_state['stored_values']['answers'])) {
        // Go-to-next-test or go-back button.
        $submit = array('langtests_nexttest');
        $value = t('Next');
        if ($next_tid == -1) {
          $submit = array('langtests_testlist');
          $value = t('Back to tests');
        }
        $form['nexttest'] = array(
          '#type' => 'submit',
          '#value' => $value,
          '#submit' => $submit,
        );
      }
    }
  }
  else {
    $form['nocontent'] = array(
      '#markup' => '<br /><br />' . t('No content.'),
    );
  }
}

/**
 * Submit handler.
 */
function langtests_testtype_display_submitter(&$form, &$form_state, $options = array(), &$testresult = array()) {
  $message = '';
  $input = array();
  $answers = array();
  $result = 0;
  $ok = 0;
  $tid = $options['tid'];

  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  if (isset($_SESSION['words' . $tid][0])) {
    $words = $_SESSION['words' . $tid][0];
  }

  foreach ($form_state['step_information'] as $k => $v) {
    if ($v['stored_values']['radios'] == $words[$k - 1]) {
      $ok++;
    }
    $answers[] = $v['stored_values']['radios'];
  }
  $form_state['stored_values']['answers'] = $answers;

  if ($ok == $form_state['values']['inset']) {
    $message = t('Congratulations! All your answers are correct!');
    $type = 'status';
    $result = 1;
  }
  else {
    $message = t('You scored !ok out of !total.',
                  array(
                    '!ok' => $ok,
                    '!total' => $form_state['values']['inset'],
                  )
                );
    $type = 'error';
  }

  $input[0] = $words;
  foreach ($answers as $v) {
    $input[] = $v;
  }

  $testresult['result'] = $result;
  $testresult['input'] = $input;

  drupal_set_message(check_plain($message), $type);

}

/**
 * Handler for prev button.
 */
function langtests_sxp_display_prev($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  if ($current_step > 1) {
    $current_step--;
    $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Handler for next button.
 */
function langtests_sxp_display_next($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  if ($current_step < $form_state['values']['inset']) {
    $current_step++;
    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = array();
    }
    $form_state['rebuild'] = TRUE;
    return;
  }
}

/**
 * Displays test result.
 */
function langtests_testtype_display_result(&$form, &$form_state, $options = array()) {
  $dictionary = $options['dictionary'];
  $input = isset($options['input']) ? $options['input'] : '';

  $form['description'] = array(
    '#type' => 'item',
    '#prefix' => '<br />',
    '#title' => t('The list of wrong answers'),
  );

  $form['table_start'] = array(
    '#markup' => '<table class="word-translation">',
  );

  $error = 0; $i = 1;
  foreach ($input[0] as $v) {
    if ($input[$i] != $v) {
      $form['word' . $i] = array(
        '#markup' => '<tr><td class="word-tr-word word-tr-word-incorrect">' . $dictionary[$v][0] . '</td>',
      );
      $ans = ($input[$i] == 'dn') ? t("I don't know") : $dictionary[$input[$i]][1];
      $form['expl' . $i] = array(
        '#markup' => '<td class="word-tr-expl word-tr-expl-incorrect">' . $ans . '</td></tr>',
      );
      $error++;
    }
    $i++;
  }
  if ($error == 0) {
    $form['description']['#title'] = '<tr><td>' . t('All answers are correct.') . '</td></tr>';
  }

  $form['table_stop'] = array(
    // Close table.
    '#markup' => '</table>',
  );
}
