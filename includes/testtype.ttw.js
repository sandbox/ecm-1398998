(function ($) {

  $(document).ready(function() {

    // Remove warning about JS
    $("#js_warning").hide();

    $("input.type-word").attr("readonly", "readonly");
    $(".word-type-letter").css("cursor", "pointer");

    var content_offset = $("#content").offset();
    var content_left = content_offset.left;
    var content_top = content_offset.top;
    var letters_height = $("#letters").height();


    // Handling mouse clicks
    $(".word-type-letter").one("click", function (e) {
      $("#letters").height(letters_height);

      var offset, start_offset;
      var input;

      input = $("input.type-word[value=\"\"]:first");
      input.val(" ");

      offset = input.offset();
      offset = {"top": offset.top - content_offset.top, "left": offset.left - content_offset.left};

      start_offset = $(this).offset();
      $(this).css({"position": "absolute"}).offset(start_offset);

      $(this).animate(offset, {
        "duration": 500,
        "complete": function () {
          $(this).position({
            of: input,
            my: "center center",
            at: "center center"
          });
          $(this).text() == "\u00A0" ? input.val(" ") : input.val($(this).text());
          $(this).css("cursor", "auto").removeClass("word-type-letter").addClass("word-type-letter-moved");
        }
      });
    });

    // Handling keyboard input
    var key_pressed = [];

    $(document)
    .keypress(function(e) {
      var key_press = String.fromCharCode(e.which);

      // Stack pressed keys
      key_pressed.push(key_press);
      if (e.which == 32 || e.which == 13) {
        return false;
      }
    })
    .keyup(function(e) {
      var k, l;
      while (key_pressed.length > 0) {
        k = key_pressed.shift();

        if (k == " ") {
          $(".word-type-letter.space:first").click();
        }
        else {
          l = $(".word-type-letter:contains(" + k + "):animated").length;
          $(".word-type-letter:contains(" + k + ")").eq(l).click();
        }
      }
      if (e.which == 13) {
        // Enter pressed and nobody moves
        if ($(".word-type-letter:animated").length == 0) {
          $("#edit-next").click();
          $("#edit-finish").click();
        }
      }
      return false;
    });


  });
} (jQuery));
