(function ($) {
  $(document).ready(function() {

    var settings = Drupal.settings['langtests'];
    var can_ddw_message = settings['can_ddw_message'];
    var init_droppables = settings['init_droppables'];

    var label_text = $("label[for=\"edit-description\"]").text();
    var js_ok = "<br>" + can_ddw_message;
    $("label[for=\"edit-description\"]").html(label_text + js_ok);

    $(".dnd-field").draggable({
      stack: ".dnd-field",
      containment: "#block-system-main form",
    });

    $(init_droppables).droppable({
      activeClass: "ui-state-active",
      accept: function(draggable) {
        return !$(this).hasClass("ui-state-dropped") ||
        ($(this).hasClass("ui-state-dropped") && draggable.hasClass("only-one"));
      },
      drop: function(event, ui) {
        $(ui.draggable).position({
          of: $(this),
          my: "center center",
          at: "center center",
          });
        $(this).addClass("ui-state-dropped");
        $(ui.draggable).addClass("only-one");
        $(this).val($(ui.draggable).text());
      },
      out: function(event, ui) {
        $(this).removeClass("ui-state-dropped").val("");
        $(ui.draggable).removeClass("only-one");
      }
    });
  });
} (jQuery));
