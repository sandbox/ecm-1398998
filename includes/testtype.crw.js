(function ($) {
  $(document).ready(function() {

    var settings = Drupal.settings['langtests'];
    var submitted = settings['submitted'];

    // Remove warning about JS
    $("#js_warning").hide();

    if (!submitted) {
      // Highlight direction -- across or down
      $("td.crossword-field").click(function() {

        var start_id = +$(this).attr("id");
        var size_x = +settings['size_x'];

        var isSelected = false;
        var isRowSelectable = false;
        var isColumnSelectable = false;
        var isColumnSelected = false;

        var id_l = start_id - 1;
        var id_r = start_id + 1;
        var id_u = start_id - size_x;
        var id_d = start_id + size_x;

        if ($("td#" + start_id).attr("abbr") != 0 && $("td#" + id_l).hasClass("crossword-field")) {
          isRowSelectable = true;
        }
        if ($("td#" + id_r).attr("abbr") != 0 && $("td#" + id_r).hasClass("crossword-field")) {
          isRowSelectable = true;
        }
        if ($("td#" + id_u).hasClass("crossword-field") || $("td#" + id_d).hasClass("crossword-field")) {
          isColumnSelectable = true;
        }

        if ($(this).hasClass("crossword-selected")) {
          isSelected = true;
        }

        // Remove all highlights
        var clear_hl = function() {
          $("td.crossword-selected").each(function() {
            $(this).find("input").removeClass("crossword-selected");
            $(this).removeClass("crossword-selected").addClass("crossword-field");
          });
        }

        // Highlighting helpers
        var highlight_l = function(id) {
          if ($("td#" + id).hasClass("crossword-field")) {
            $("td#" + id).removeClass("crossword-field").addClass("crossword-selected");
            $("input#edit-textfield-" + id).addClass("crossword-selected");
            if ($("td#" + id).attr("abbr") != 0) {
              id--;
              highlight_l(id);
            }
          }
        }

        var highlight_r = function(id) {
          id++;
          if ($("td#" + id).attr("abbr") != 0 && $("td#" + id).hasClass("crossword-field")) {
            $("td#" + id).removeClass("crossword-field").addClass("crossword-selected");
            $("input#edit-textfield-" + id).addClass("crossword-selected");
            highlight_r(id);
          }
        }

        var highlight_u = function(id) {
          if ($("td#" + id).hasClass("crossword-field")) {
            $("td#" + id).removeClass("crossword-field").addClass("crossword-selected");
            $("input#edit-textfield-" + id).addClass("crossword-selected");
            id = id - size_x;
            highlight_u(id);
          }
        }

        var highlight_d = function(id) {
          id = id + size_x;
          if ($("td#" + id).hasClass("crossword-field")) {
            $("td#" + id).removeClass("crossword-field").addClass("crossword-selected");
            $("input#edit-textfield-" + id).addClass("crossword-selected");
            highlight_d(id);
          }
        }

        if (isSelected) {

          if ($("td#" + id_u).hasClass("crossword-selected") || $("td#" + id_d).hasClass("crossword-selected")) {
            isColumnSelected = true;
          }
          if (isColumnSelected && isRowSelectable) {
            // Column selected
            clear_hl();
            highlight_l(start_id);
            highlight_r(start_id);
          }
          else if (!isColumnSelected && isColumnSelectable) {
            // Row selected
            clear_hl();
            highlight_u(start_id);
            highlight_d(start_id);
          }
        }
        else {
          // The cell is not selected

          clear_hl();

          if (isRowSelectable) {
            highlight_l(start_id);
            highlight_r(start_id);
          }
          else if (isColumnSelectable) {
            highlight_u(start_id);
            highlight_d(start_id);
          }
        }

      });

      // Handling keyboard input
      var key_pressed = [];

      var key_accept = function (k) {
        if (k == 32 || (k >= 48 && k <= 57) || (k >= 65 && k <= 90)
             || (k >= 97 && k <= 122) || (k >= 186 && k <= 192)
             || (k >= 219 && k <= 222) || k >= 1025) {
          return true;
        }
        else {
          return false;
        }
      }

      $("input.crossword-form-text")
      .focus(function() {
        $(this).select();
      })
      // Needed for Safary and Chrome
      .mouseup(function(e) {
        e.preventDefault();
      })
      .keydown(function(e) {
        if (!key_accept(e.which)) {
          // Ignore Enter, Tab, Esc and some other keys
          return false;
        }
      })
      .keypress(function(e) {
        if (key_accept(e.which)) {
          var key_press = String.fromCharCode(e.which);
          // Stack pressed keys
          key_pressed.push(key_press);
        }
        else {
          return false;
        }
      })
      .keyup(function(e) {
        var start_id = +$(this).closest("td").attr("id");
        var id_l = start_id - 1;
        var id_r = start_id + 1;
        var size_x = +settings['size_x'];
        var id_u = start_id - size_x;
        var id_d = start_id + size_x;
        var isColumnSelected = false;

        if ($("td#" + id_u).hasClass("crossword-selected") || $("td#" + id_d).hasClass("crossword-selected")) {
          isColumnSelected = true;
        }

        var input_id_name = settings['input_id_name'];

        // Backspace pressed
        if (e.which == 8) {
          if (isColumnSelected) {
            $(this).val("");
            $(input_id_name + id_u).focus();
          }
          else if ($("td#" + start_id).attr("abbr") != 0) {
            $(this).val("");
            $(input_id_name + id_l).focus();
          }
        }

        // Del pressed
        if (e.which == 46) {
          $(this).val("").focus();
        }

        // Letter or digit key pressed
        if (key_pressed.length > 0) {
          var next_cell;
          $(this).val(key_pressed.shift());
          next_cell = id_r;
          if (isColumnSelected) {
            next_cell = id_d;
          }
          else if ($(this).closest("td").attr("abbr") == (size_x - 1)) {
            next_cell = start_id;
          }
          $(input_id_name + next_cell).focus();
        }

        return false;
      });
    }
  });
} (jQuery));
