(function ($) {
  $(document).ready(function() {

    var settings = Drupal.settings['langtests'];

    var submitted = settings['submitted'];
    var correct_index = settings['correct_index'].split(",");
    var right_keys = settings['right_keys'].split(",");

    // Convert strings to numbers
    $.each(correct_index, function(i, v) {
      correct_index[i] = +v;
    });
    $.each(right_keys, function(i, v) {
      right_keys[i] = +v;
    });

    // Hide inputs
    $(".matcher-hidden").each(function() { $(this).hide(); });

    // Get coordinates for holder and set its position
    var offset = $("table.matcher").offset();
    var width = $("table.matcher").width();
    var height = $("table.matcher").height();
    $("#mtrholder").offset(offset);
    var radius = 8;

    // The last coordinates of moved curve
    var upX;
    var upY;

    // The mouse pointer coordinates
    var mouseX;
    var mouseY;
    $(document).mousemove(function(em){
      mouseX = em.pageX - offset.left;
      mouseY = em.pageY - offset.top;
    });

    var r = Raphael("mtrholder", width, height);

    function curve(x, y, ax, ay, bx, by, zx, zy, colour) {
      var color = colour || Raphael.getColor();
      var discattr = {fill: color, stroke: "none"};

      var path = [["M", x, y], ["C", ax, ay, bx, by, zx, zy]];
      var curve = r.path(path).attr({stroke: color || Raphael.getColor(), "stroke-width": 4, "stroke-linecap": "round"});
      var control = r.circle(zx, zy, radius).attr(discattr);
      if (!submitted) {
        control.attr("cursor", "pointer");
      }
      r.circle(x, y, radius).attr(discattr);

      control.update = function (x, y, glue) {
        var X = this.attr("cx") + x;
        var Y = this.attr("cy") + y;
        var bX = path[1][3] + x;
        var bY = path[1][4] + y;

        if (Y < radius || mouseY < radius) {
          Y = radius; bY = radius;
        } else if (Y > r.height - radius || mouseY > r.height - radius) {
          Y = r.height - radius; bY = r.height - radius;
        } else if (!glue) {
          Y = mouseY; bY = mouseY;
        }

        if (X > curve_target || mouseX > curve_target) {
          X = curve_target; bX = curve_target - power;
        } else if (X < curve_right || mouseX < curve_right) {
          X = curve_right; bX = curve_right - power;
        } else if (!glue) {
          X = mouseX; bX = mouseX - power;
        }

        this.attr({cx: X, cy: Y});

        path[1][3] = bX;
        path[1][4] = bY;
        path[1][5] = X;
        path[1][6] = Y;
        upX = X;
        upY = Y;

        curve.attr({path: path});
      };

      if (!submitted) {
        control.drag(move, start, up);
      }
    }

    // The degree of curvature
    var power = 70;

    var curve_left = $(".rathtd").offset().left - offset.left - radius / 2;
    var curve_right = curve_left + 80;
    var curve_target = curve_left + $(".rathtd").outerWidth() + radius;
    var curve_y = [];

    var y = 0;
    $(".rathtd").each(function() {
      var h = $(this).outerHeight(true);
      curve_y.push(y + h / 2);
      y = y + h;
    });

    function move(dx, dy) {
      this.update(dx - (this.dx || 0), dy - (this.dy || 0), false);
      this.dx = dx;
      this.dy = dy;
    }
    function start() {
      this.dx = this.dy = 0;
    }
    function up() {
      var glue = false;
      var id = +this.id;

      if (upX < (curve_target + 4 * radius) && upX > (curve_target - 4 * radius)) {
        var delta_x = curve_target - upX;
        var delta_y = 0;
        $.each(curve_y, function(index, y) {
          if (upY < (y + 1.5 * radius) && upY > (y - 1.5 * radius)) {
            delta_y = y - upY;
            glue = true;
            id = (id - 1) / 4 + 1;
            $("#edit-f" + (index + 1)).val(id);
          }
        });
        if (glue) {
          this.update(delta_x, delta_y, glue);
        }
      }
    }

    var color = ["hsb(.8, .75, .75)", "hsb(.6, .75, .75)",
                 "hsb(.3, .75, .75)"];

    if (!submitted) {
      $.each(curve_y, function(index, y) {
        var colour = (index in color) ? color[index] : null;
        // Print curves
          curve(curve_left, y, (curve_left + power), y,
               (curve_right - power), y, curve_right, y, colour);
        // Print targets
        r.circle(curve_target, y, radius)
            .attr({fill: "#999", stroke: "none"}).toBack();
      });
    }
    else {
      // Print correct curves
      $.each(correct_index, function(i, v) {
        var v = v - 1;
        var colour = (v in color) ? color[v] : null;
        y = curve_y[v];
        var tIndex = $.inArray((v + 1), right_keys);
        var tY = curve_y[tIndex];
        curve(curve_left, y, (curve_left + power), y,
               (curve_target - power), tY, curve_target, tY, colour);
      });
    }

  });
} (jQuery));
