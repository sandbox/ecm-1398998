(function ($) {
  $(document).ready(function() {
    var settings = Drupal.settings["langtests"];

    // Replace warning about JS
    var t = settings["enter_letters_message"];
    var yes = settings["yes"];
    var no = settings["no"];

    $("span.fieldset-legend").html(t);

    // Enable fields in the grid
    $("input.wordsearch-form-text").removeAttr("readonly");

    var isMouseDown = false;
    var class_hl = "wordsearch-admin-over";
    var class_nhl = "wordsearch-admin-out";
    var class_hover = "wordsearch-admin-hover";
    var selection = [];
    var answer, word, letter;

    // Initialize dialog
    var dialog = $("<div id=\"dia\"></div>")
      .html("")
      .dialog({
        autoOpen: false,
        title: settings["accept_the_word_message"],
        modal: true,
        buttons: [
          {
            text: yes,
            click: function() { accept_yes(); $(this).dialog("close"); }
          },
          {
            text: no,
            click: function() { accept_no(); $(this).dialog("close"); }
          }
        ]
      });

    function accept_yes () {
      $("#answer-no").hide();
      $("#answer-words").append("<br>");
      answer = "";
      $.each(selection, function(index, value) { answer = answer + value.id + " "; });
      answer = $.trim(answer) + "\n";
      $("#edit-fieldset-answers").append(answer);
    }
    function accept_no () {
      var val = $("#answer-words").html();
      val = val.substring(0, val.length - selection.length);
      if (selection.length > 1 || selection[0]['letter'] != "*") {
        $("#answer-words").empty().append(val);
      }

      $.each(selection, function(index, value) {
        $("#" + value.id).toggleClass(class_hl, value.hl).toggleClass(class_nhl, !value.hl);
      });
    }

    $("td.wordsearch-admin-td")
      .mousedown(function (e) {
        if (e.which == 1) {
          // Catch only left button
          isMouseDown = true;
          selection = [];
          letter = $(this).find("input").val() ? $(this).find("input").val() : "*";
          selection.push({
            "id":$(this).attr("id"),
            "letter":letter,
            "hl":$(this).hasClass(class_hl)
          });

          $(this).addClass(class_hl).removeClass(class_nhl);
          $(this).find("input").focus();

          // The first letter
          if (letter != "*") {
            $("#answer-words").append(letter);
          }

          // Prevent text selection
          return false;
        }
      })
      .hover(function () {
        // Will prevent the repetitions of characters
        var stepback = true;
        var cell_id = $(this).attr("id");
        if (isMouseDown) {
          $.each(selection, function (index, value) {
            stepback = (value.id == cell_id) ? false : true;
            return stepback;
          });
          if (stepback) {
            letter = $(this).find("input").val() ? $(this).find("input").val() : "*";
            selection.push({
              "id":$(this).attr("id"),
              "letter":letter,
              "hl":$(this).hasClass(class_hl)
            });
            $(this).addClass(class_hl).removeClass(class_nhl);
            if (selection.length == 2 && selection[0].letter == "*") {
              $("#answer-words").append("*");
            }
            // The rest of letters
            $("#answer-words").append(letter);
          }
        }
        else {
          $(this).addClass(class_hover);
        }
      }, function () {
        $(this).removeClass(class_hover);
      })

      // Fix IE
      .bind("selectstart", function () {
        return false;
      });

    $(document).mouseup(function () {
      if (isMouseDown) {
        isMouseDown = false;

        if (selection.length == 1) { accept_no(); }
        if (selection.length > 1) {
          word = "";
          $.each(selection, function(index, value) {
            word = word + value.letter;
          });
          dialog.html("<center>" + settings["wanna_this_word_message"] + "<br /><br /><strong>" + word + "</strong></center>")
                .dialog("open");
        }
      }
    });

    $("#clear-button").click(function () {
      $("#edit-fieldset-answers").empty();
      $("#answer-words").empty();
      $(".wordsearch-admin-td").addClass(class_nhl).removeClass(class_hl);
    });

  });

} (jQuery));
