(function ($) {
  $(document).ready(function() {
    var settings = Drupal.settings["langtests"];

    var count_answers = settings["count_answers"];
    var words_left = settings["words_left"] + " ";
    $("label[for=edit-wordsearch]").text(words_left + count_answers);

    // Replace warning about JS
    var t = settings["highlight_words_message"];
    $(".form-type-textarea>.description").html(t);

    $("#edit-wordsearch").attr("readonly", "readonly");

    // Make an array full of the correct answers
    var correctJSON = [], correct = [];
    correctJSON = $.parseJSON(settings["correct_answers_json"]);
    $.each(correctJSON, function(index, value) {
      value = value + " ";
      correct.push(value);
    });

    var isMouseDown = false, isHighlighted;
    var class_hl = "wordsearch-over";
    var class_nhl = "wordsearch-out";
    var class_hover = "wordsearch-hover";
    var selection = [];
    var answer;

    $("td.wordsearch-td")
      .mousedown(function (e) {
        if (e.which == 1) {
          // Catch only left button
          isMouseDown = true;
          selection = [];
          selection.push({"id":$(this).attr("id"), "hl":$(this).hasClass(class_hl)});

          $(this).addClass(class_hl).removeClass(class_nhl);

          // The first letter
          $("#edit-wordsearch").append($(this).text());

          // Prevent text selection
          return false;
        }
      })
      .hover(function () {
        // Will prevent the repetitions of characters
        var stepback = true;
        var cell_id = $(this).attr("id");
        if (isMouseDown) {
          $.each(selection, function (index, value) {
            stepback = (value.id == cell_id) ? false : true;
            return stepback;
          });
          if (stepback) {
            selection.push({"id":$(this).attr("id"), "hl":$(this).hasClass(class_hl)});
            $(this).addClass(class_hl).removeClass(class_nhl);
            // The rest of letters
            $("#edit-wordsearch").append($(this).text());
          }
        }
        else {
          $(this).addClass(class_hover);
        }
      }, function () {
        $(this).removeClass(class_hover);
      })

      // Fix IE
      .bind("selectstart", function () {
        return false;
      });

    $(document).mouseup(function () {
      if (isMouseDown) {
        isMouseDown = false;

        answer = "";
        $.each(selection, function(index, value) {
          answer = answer + value.id + " ";
        });

        var index = $.inArray(answer, correct);
        if (index > -1) {
          count_answers--;
          $("label[for=edit-wordsearch]").text(words_left + count_answers);

          if (count_answers == 0) {
            var t = settings["press_check_message"];
            $(".form-type-textarea>.description").html(t);
          }

          delete correct[index];
          $("#edit-wordsearch").append("\n");
        }
        else {
          var val = $("#edit-wordsearch").val();
          val = val.substring(0, val.length - selection.length);
          $("#edit-wordsearch").empty().append(val);

          $.each(selection, function(index, value) {
            $("#" + value.id).toggleClass(class_hl, value.hl).toggleClass(class_nhl, !value.hl);
          });
        }
      }
    });

  });

} (jQuery));
