This module allows language teachers to create tests and offer them
to their students. In turn, the students can join the teacher's group
and take tests. The results will be available to the teacher.

Now it is possible to create 9 types of tests.

Drop-down lists
  Choose the correct answer from the drop-down list.
Input fields
  Enter the answers in the input fields.
Radios and checkboxes
  Choose the correct answer to the questions.
Drag-and-drop words
  Put the words in the right places by dragging them with your mouse.
Wordsearch
  Look at the wordsearch and find words.
Crossword
  Complete the crossword.
Matcher
  Match the words on the left to the information on the right.
Select explanation
  Select the correct explanation of the word.
Type the word
  Type the word that corresponds to the description given.

To create a Matcher type tests you need a Raphaël — JavaScript Library.
You can download it at
http://github.com/DmitryBaranovskiy/raphael/raw/master/raphael-min.js
Just put the file raphael-min.js into the sites/all/libraries/raphael
directory (or sites/example.com/libraries/raphael if you have a
multi-site installation). See http://drupal.org/node/1440066 for more
details.

For more information about Raphaël library, visit http://raphaeljs.com/

Test samples can be found on the demo site at http://langtests.vernex.ru
