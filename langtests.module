<?php
/**
 * @file
 * Forms for language tests
 */

/**
 * Implements hook_menu().
 */
function langtests_menu() {
  $items = array();
  $items['langtests'] = array(
    'title' => 'Language Tests',
    'page callback' => 'langtests_list',
    'access callback' => TRUE,
    'menu_name' => 'main-menu',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/langtests-display.inc',
  );
  $items['langtests/group'] = array(
    'title' => "My teachers' tests",
    'access callback' => 'langtests_student_access',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );
  $items['langtests/public'] = array(
    'title' => 'Public tests',
    'page callback' => 'langtests_list_public',
    'page arguments' => array(2),
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/langtests-display.inc',
    'weight' => 1,
  );
  $items['langtests/results'] = array(
    'title' => 'My results',
    'page callback' => 'langtests_list_results',
    'access callback' => 'langtests_student_access',
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/langtests-display.inc',
    'weight' => 2,
  );
  $items['langtests/unsubscribe/%'] = array(
    'title' => 'Unsubscribe',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_unsubscribe', 2),
    'access callback' => 'langtests_student_access',
    'type' => MENU_CALLBACK,
    'file' => 'includes/langtests-display.inc',
  );
  $items['langtests/%'] = array(
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_set_display', 1, 2),
    'access callback' => 'langtests_set_access',
    'access arguments' => array(1, 2),
    'type' => MENU_CALLBACK,
    'file' => 'includes/langtests-display.inc',
  );
  $items['admin/langtests'] = array(
    'title' => 'My Language Tests',
    'description' => 'Manage my language tests',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_admin', 2),
    'access callback' => 'langtests_admin_access',
    'file' => 'includes/langtests-admin.inc',
    'menu_name' => 'management',
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/langtests/create'] = array(
    'title' => 'New test set',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_admin_create'),
    'access callback' => 'langtests_admin_access',
    'file' => 'includes/langtests-admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/langtests/delete/%'] = array(
    'title' => 'Delete the test set',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_admin_delete', 3),
    'access callback' => 'langtests_admin_access',
    'file' => 'includes/langtests-admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/students'] = array(
    'title' => 'My students',
    'description' => 'Manage my students',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_students', 2, 3),
    'access callback' => 'langtests_admin_access',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/langtests-admin.inc',
    'menu_name' => 'management',
  );
  $items['admin/students/showset'] = array(
    'title' => 'Test set results',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_showset', 3, 4, 5),
    'access callback' => 'langtests_admin_access',
    'type' => MENU_CALLBACK,
    'file' => 'includes/langtests-admin.inc',
  );
  $items['admin/langtests/testitem'] = array(
    'title' => 'Task',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('langtests_testitem', 3, 4),
    'access callback' => 'langtests_admin_access',
    'type' => MENU_CALLBACK,
    'file' => 'includes/langtests-admin.inc',
  );

  return $items;
}

/**
 * Access callback function for langtests/% menu item.
 *
 * @param int $sid
 *   Test set ID
 * @param int $tid
 *   Test item ID
 */
function langtests_set_access($sid, $tid = NULL) {
  global $user;
  $good_sid = FALSE;
  $good_tid = FALSE;
  $access = FALSE;

  if (is_numeric($sid)) {
    $query = db_select('langtests_sets', 's');
    $query
      ->fields('s')
      ->condition('s.sid', $sid);
    $result = $query->execute();
    $rescount = $result->rowCount();

    if ($rescount == 1) {
      $record = $result->fetchAssoc();
      if (($record['enabled'] && $record['public']) || ($record['uid'] == $user->uid)) {
        $good_sid = TRUE;
      }
      if ($record['enabled'] && !$good_sid) {
        $query = db_select('langtests_groups', 'g');
        $query
          ->fields('g')
          ->condition('g.student_uid', $user->uid)
          ->condition('g.teacher_uid', $record['uid'])
          ->condition('g.status', 1);
        $gr_result = $query->execute();
        $gr_count = $gr_result->rowCount();

        if ($gr_count == 1) {
          $good_sid = TRUE;
        }
      }
    }
  }

  // We don't want to display disabled items and items from other sets.
  if ($good_sid && is_numeric($tid)) {
    $tid_query = db_select('langtests_items', 'i');
    $tid_query
      ->fields('i')
      ->condition('i.tid', $tid)
      ->condition('i.sid', $sid)
      ->condition('i.enabled', 1);
    $tid_result = $tid_query->execute();
    $tid_count = $tid_result->rowCount();

    if ($tid_count == 1) {
      $good_tid = TRUE;
    }
  }

  // Let access to page.
  if (($good_sid && $tid == '') || ($good_sid && $good_tid)) {
    $access = TRUE;
  }

  return $access;
}

/**
 * Access callback function for admin/langtests/% menu item.
 */
function langtests_admin_access() {
  global $user;
  $access = FALSE;

  if ($user->uid) {
    $curruser = user_load($user->uid);
    $if_teacher = field_get_items('user', $curruser, 'field_teacher');
    $interface_teacher = ($if_teacher) ? $if_teacher[0]['value'] : FALSE;
    if ($interface_teacher || user_access('access administration pages')) {
      $access = TRUE;
    }
  }

  return $access;
}

/**
 * Access callback function for langtests/group and langtests/results.
 */
function langtests_student_access() {
  global $user;
  $access = FALSE;

  if ($user->uid) {
    $curruser = user_load($user->uid);
    $if_student = field_get_items('user', $curruser, 'field_student');
    $interface_student = ($if_student) ? $if_student[0]['value'] : FALSE;
    if ($interface_student) {
      $access = TRUE;
    }
  }

  return $access;
}

/**
 * Implements hook__theme_registry_alter().
 */
function langtests_theme_registry_alter(&$theme_registry) {
  $theme_registry['form_element']['function'] = 'langtests_form_element';
}

/**
 * Theming function for forms.
 */
function langtests_form_element($variables) {
  $output = '';
  if (isset($variables['element']['#inline']) && $variables['element']['#inline']) {
    /*
     * Show form element inline, wrapped in a SPAN container
     * (instead of DIV).
     */
    $element = &$variables['element'];
    $t = get_t();
    $element += array(
      '#title_display' => 'before',
    );
    if (isset($element['#markup']) && !empty($element['#id'])) {
      $attributes['id'] = $element['#id'];
    }
    $attributes['class'] = array('form-item');
    if (!empty($element['#type'])) {
      $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
    }
    if (!empty($element['#name'])) {
      $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(
        ' ' => '-',
        '_' => '-',
        '[' => '-',
        ']' => '',
      ));
    }
    if (!empty($element['#attributes']['disabled'])) {
      $attributes['class'][] = 'form-disabled';
    }
    $output = '<span' . drupal_attributes($attributes) . '>' . "\n";
    if (!isset($element['#title'])) {
      $element['#title_display'] = 'none';
    }
    $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
    $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

    switch ($element['#title_display']) {
      case 'before':
      case 'invisible':
        $output .= ' ' . theme('form_element_label', $variables);
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;

      case 'after':
        $output .= ' ' . $prefix . $element['#children'] . $suffix;
        $output .= ' ' . theme('form_element_label', $variables) . "\n";
        break;

      case 'none':
      case 'attribute':
        $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
        break;
    }

    if (!empty($element['#description'])) {
      $output .= '<div class="description">' . $element['#description'] . "</div>\n";
    }
    $output .= "</span>\n";
  }

  else {
    // Show form with default theming.
    $output = theme_form_element($variables);
  }

  return $output;
}

/**
 * Implements hook_init().
 */
function langtests_init() {
  drupal_add_css(drupal_get_path('module', 'langtests') . '/css/langtests.css');

  // Init global variables for the module.
  global $_langtests_levels;
  $_langtests_levels = variable_get('langtests_levels');

  global $_langtests_languages;
  include_once DRUPAL_ROOT . '/includes/iso.inc';
  $langs = (_locale_get_predefined_list());
  foreach ($langs as $k => $v) {
    $_langtests_languages[] = (isset($v[1])) ? $v[0] . ' | ' . $v[1] : $v[0];
  }
  sort($_langtests_languages);

  global $_langtests_types;
  $_langtests_types = variable_get('langtests_types');
}

/**
 * Implements hook_permission().
 */
function langtests_permission() {
  return array(
    'make language tests public' => array(
      'title' => t('Make language tests public'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_theme().
 */
function langtests_theme() {
  return array(
    'langtests_admin' => array(
      'file' => 'includes/langtests-admin.inc',
      'render element' => 'form',
    ),
    'langtests_list' => array(
      'file' => 'includes/langtests-display.inc',
    ),
    'langtests_list_results' => array(
      'file' => 'includes/langtests-display.inc',
    ),
  );
}

/**
 * Implements hook_user_presave().
 *
 * Change role depending on the value of field_teacher
 */
function langtests_user_presave(&$edit, $account, $category) {
  if (isset($edit['field_teacher'])) {
    $field_teacher = $edit['field_teacher']['und'][0]['value'];
    $role = user_role_load_by_name('teacher');
    if ($field_teacher) {
      // Enable teacher's interface.
      if ($role) {
        $rid = $role->rid;
        $edit['roles'][$rid] = $rid;
      }
    }
    else {
      // Disable teacher's interface.
      if ($role) {
        $rid = $role->rid;
        $edit['roles'][$rid] = 0;
      }
    }
  }
}

/**
 * Implements hook_mail().
 */
function langtests_mail($key, &$message, $params) {
  global $user;

  // This $options array is used later in the t() calls for subject
  // and body to ensure the proper translation takes effect.
  $options = array(
    'langcode' => $message['language']->language,
  );

  switch ($key) {
    case 'joingroup_request':
      $message['subject'] = t('@site-name: Request to join your group', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;

    case 'joingroup_allow':
      $message['subject'] = t('@site-name: Your request to join the group has been approved', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;

    case 'joingroup_decline':
      $message['subject'] = t('@site-name: Your request to join the group has been declined', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;

    case 'joingroup_spam':
      $message['subject'] = t('@site-name: Abuse report', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = t('This is an abuse report from the teacher @name.', array('@name' => drupal_strtoupper($user->name)), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;

    case 'joingroup_message':
      $message['subject'] = t('@site-name: Message from your teacher', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;

    case 'joingroup_exclude':
      $message['subject'] = t('@site-name: You have been excluded from the group', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;

    case 'joingroup_unsubscribe':
      $message['subject'] = t('@site-name: A student has unsubscribed from your group', array('@site-name' => variable_get('site_name', 'Langtests')), $options);
      $message['body'][] = check_plain($params['message']);
      $message['body'][] = check_plain($params['signature']);
      break;
  }
}

/**
 * Sends an e-mail.
 *
 * @param array $values
 *   An array with keys: pattern, to, from, message, signature,
 *   success, failure.
 */
function langtests_mail_send($values) {
  $module = 'langtests';
  $key = $values['pattern'];

  $to = $values['to'];
  $from = isset($values['from']) ? $values['from'] : variable_get('site_mail', 'admin@example.com');

  $params['message'] = isset($values['message']) ? $values['message'] : '';
  $params['signature'] = isset($values['signature']) ? $values['signature'] : '';

  $success = isset($values['success']) ? $values['success'] : t('Your message has been sent.');
  $failure = isset($values['failure']) ? $values['failure'] : t('There was a problem while sending your message and it was not sent.');

  // The language of the e-mail. This will one of three values:
  // * user_preferred_language(): Used for sending mail to a particular website
  // user, so that the mail appears in their preferred language.
  // * global $language: Used when sending a mail back to the user currently
  // viewing the site. This will send it in the language they're currently
  // using.
  // * language_default(): Used when sending mail to a pre-existing, 'neutral'
  // address, such as the system e-mail address, or when you're unsure of the
  // language preferences of the intended recipient.
  global $language;

  $send = TRUE;
  // Send the mail, and check for success.
  $result = drupal_mail($module, $key, $to, $language, $params, $from, $send);
  if ($result['result'] == TRUE) {
    drupal_set_message(check_plain($success));
  }
  else {
    drupal_set_message(check_plain($failure), 'error');
  }
}

/**
 * Implements hook_help().
 */
function langtests_help($path, $arg) {
  $output = '';
  switch ($path) {
    case 'admin/help#langtests':
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>';
      $output .= t('The <em>Language Tests</em> module allows language teachers to create tests and offer them to their students.') . ' ';
      $output .= t("In turn, the students can join the teacher's group and take tests. The results will be available to the teacher.");
      $output .= '</p>';

      $output .= '<h3>' . t('Types of tests') . '</h3>';
      $output .= t('Now you can create 9 types of tests:');
      $output .= '</p>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Drop-down lists') . '</dt>';
      $output .= '<dd>' . t('Choose the correct answer from the drop-down list.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Input fields') . '</dt>';
      $output .= '<dd>' . t('Enter the answers in the input fields.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Radios and checkboxes') . '</dt>';
      $output .= '<dd>' . t('Choose the correct answer to the questions.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Drag-and-drop words') . '</dt>';
      $output .= '<dd>' . t('Put the words in the right places by dragging them with your mouse.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Wordsearch') . '</dt>';
      $output .= '<dd>' . t('Look at the wordsearch and find words.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Crossword') . '</dt>';
      $output .= '<dd>' . t('Complete the crossword.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Matcher') . '</dt>';
      $output .= '<dd>' . t('Match the words on the left to the information on the right.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Select definition') . '</dt>';
      $output .= '<dd>' . t('Select the correct definition of the word.') . '</dd>';
      $output .= '</dl>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Type the word') . '</dt>';
      $output .= '<dd>' . t('Type the word that corresponds to the definition given.') . '</dd>';
      $output .= '</dl>';

      $output .= '<p>';
      $output .= t('Test samples and detailed instructions are available on the demo site at <a href="http://langtests.vernex.ru" target=_blank>langtests.vernex.ru</a>.');
      $output .= '</p>';

      $output .= t('To create a Matcher type tests you need a <b>Raphaël — JavaScript Library</b>.');
      $output .= ' ';
      $output .= t('You can download it <a href="http://github.com/DmitryBaranovskiy/raphael/raw/master/raphael-min.js">here</a>.');
      $output .= '<br />';
      $output .= t('Just put the file <b>raphael-min.js</b> into the <i>sites/all/libraries/raphael</i> directory (or <i>sites/example.com/libraries/raphael</i> if you have a multi-site installation).');
      $output .= ' ';
      $output .= t('See <a href="http://drupal.org/node/1440066" target=_blank>Installing an external library that is required by a contributed module</a> for more details.');
      $output .= '<p>';

      break;
  }

  return $output;
}

/**
 * Alter menu to make help page accessible for teachers.
 */
function langtests_menu_alter(&$items) {
  $items['admin/help/langtests']['access callback'] = 'langtests_admin_access';
}
